<?php

class AW_Blog_Block_Custom extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
	
    protected static $_catUriParam = AW_Blog_Helper_Data::CATEGORY_URI_PARAM;

    protected static $_postUriParam = AW_Blog_Helper_Data::POST_URI_PARAM;

    protected static $_tagUriParam = AW_Blog_Helper_Data::TAG_URI_PARAM;
    
	public function __construct() 
    {
        parent::__construct();
        $this->setCollection($this->getRecent());
    }
	protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'cat_cms.pager');
        $pager->setAvailableLimit(array(6=>6,12=>12,24=>24,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getRecent()
    {
		$_helper = Mage::helper('blog');
        $collection = Mage::getModel('blog/blog')->getCollection()
            ->addPresentFilter()
            ->addEnableFilter(AW_Blog_Model_Status::STATUS_ENABLED)
            ->addStoreFilter()
            ->setOrder('created_time', 'desc')
        ;
			/* $collection->getSelect()->limit($this->getBlocksCount()); */
		
         /* if ($this->getBlocksCount()) { */
			/* $collection->setPageSize($this->getBlocksCount()); */
        /* } else {
            $collection->setPageSize(Mage::helper('blog')->getRecentPage());
        }  */

        /* if ($collection && $this->getData('categories')) { */
            $collection->addCatFilter($this->getData('categories'));
       /*  } */
			
		$category=Mage::getModel('blog/cat')->load($this->getData('categories'));
        foreach ($collection as $item) {
			$item->setAddress($this->getBlogUrl(null, array(
                             self::$_catUriParam  => $category->getIdentifier(),
                             self::$_postUriParam => $item->getIdentifier()
            )));
        }
        return $collection;
    }
	 public function getBlogUrl($route = null, $params = array())
    {
		
       /*  $blogRoute = self::$_helper->getRoute(); */
		$blogRoute="";
        if (is_array($route)) {
            foreach ($route as $item) {
                $item = urlencode($item);
                /* $blogRoute .= "/{$item}"; */
				$blogRoute="{$item}";
            }
        } else {
           /*  $blogRoute .= "/{$route}"; */
			$blogRoute .= "{$route}";
        }
        foreach ($params as $key => $value) {
            $value = urlencode($value);
            $blogRoute .= "{$value}/";
        }

        return $this->getUrl($blogRoute);
    }
	protected function _toHtml()
    {
		$_helper = Mage::helper('blog');
        $this->setTemplate($this->getData('template'));
        if ($_helper->getEnabled()) {
            return $this->setData('blog_widget_recent_count', $this->getBlocksCount())->renderView();
        }
    }
}
