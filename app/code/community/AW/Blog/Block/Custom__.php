<?php

class AW_Blog_Block_Custom extends AW_Blog_Block_Menu_Sidebar implements Mage_Widget_Block_Interface
{
    protected function _toHtml()
    {
         $this->setTemplate($this->getData('template'));
        if ($this->_helper()->getEnabled()) {
            return $this->setData('blog_widget_recent_count', $this->getBlocksCount())->renderView();
        }
    }

     public function getRecent()
    {
		$_helper = Mage::helper('blog');
        $collection = Mage::getModel('blog/blog')->getCollection()
            ->addPresentFilter()
            ->addEnableFilter(AW_Blog_Model_Status::STATUS_ENABLED)
            ->addStoreFilter()
            ->setOrder('created_time', 'desc')
        ;
		
         if ($this->getBlocksCount()) {
            $collection->setPageSize($this->getBlocksCount());
        } else {
            $collection->setPageSize(Mage::helper('blog')->getRecentPage());
        } 

        if ($collection && $this->getData('categories')) {
            $collection->addCatsFilter($this->getData('categories'));
        }
		
		$category=Mage::getModel('blog/cat')->load($this->getData('categories'));
        foreach ($collection as $item) {
			$item->setAddress($this->getBlogUrl(null, array(
                             self::$_catUriParam  => $category->getIdentifier(),
                             self::$_postUriParam => $item->getIdentifier()
            )));
        }
        return $collection;
    }
	public function getBlogUrl($route = null, $params = array())
    {
		
       /*  $blogRoute = self::$_helper->getRoute(); */
		$blogRoute="";
        if (is_array($route)) {
            foreach ($route as $item) {
                $item = urlencode($item);
                /* $blogRoute .= "/{$item}"; */
				$blogRoute="{$item}";
            }
        } else {
           /*  $blogRoute .= "/{$route}"; */
			$blogRoute .= "{$route}";
        }
        foreach ($params as $key => $value) {
            $value = urlencode($value);
            $blogRoute .= "{$value}/";
        }

        return $this->getUrl($blogRoute);
    }
}
