<?php

class AW_Blog_Block_Catc extends AW_Blog_Block_Blog
{
	protected function _getCatBlock(){
		$category = $this->getCategory();
        if (!$this->_tplProcessor){
            $this->_tplProcessor = Mage::helper('cms')->getBlockTemplateProcessor();
        }
        return $this->_tplProcessor->filter( trim($category->getData('s_content')));
    }
    public function getCategory()
    {
        return Mage::getSingleton('blog/cat');
    }

    protected function _prepareLayout()
    {
        $post = $this->getCategory();
        $breadcrumbs = $this->getCrumbs();
        if ($breadcrumbs) {
            $breadcrumbs->addCrumb(
                'blog',
                array(
                     'label' => self::$_helper->getTitle(),
                     'title' => $this->__('Return to %s', self::$_helper->getTitle()),
                     'link'  => $this->getBlogUrl(),
                )
            );
            $breadcrumbs->addCrumb('blog_page', array('label' => $post->getTitle(), 'title' => $post->getTitle()));
        }

        parent::_prepareMetaData($post);
    }

}
