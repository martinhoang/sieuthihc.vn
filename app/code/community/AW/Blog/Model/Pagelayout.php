<?php

class AW_Blog_Model_Pagelayout extends Varien_Object
{
    const COLUMN_1 			= 	'1column';
    const COLUMN_2_LEFT		= 	'2columns-left';
    const COLUMN_2_RIGHT   	= 	'2columns-right';
	const COLUMN_3			=	'3columns';
	const EMPTY_PAGE		=   'empty';

    static public function toOptionArray()
    {
        return array(
			array('value'=>self::COLUMN_1,'label'=>Mage::helper('blog')->__('1 column')),
			array('value'=>self::COLUMN_2_LEFT,'label'=>Mage::helper('blog')->__('2 columns left')),
			array('value'=>self::COLUMN_2_RIGHT,'label'=>Mage::helper('blog')->__('2 columns right')),
			array('value'=>self::COLUMN_3,'label'=>Mage::helper('blog')->__('3 columns')),
			array('value'=>self::EMPTY_PAGE,'label'=>Mage::helper('blog')->__('Page empty')),
        );
    }
}