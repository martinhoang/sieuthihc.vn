<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('blog/cat')} ADD  `page_layout` varchar( 255 ) NOT NULL default '1column';
");
$installer->endSetup();