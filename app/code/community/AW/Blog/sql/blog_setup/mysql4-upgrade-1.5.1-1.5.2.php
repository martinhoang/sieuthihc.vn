<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('blog/cat')} ADD  `s_content` text NOT NULL;
");
$installer->endSetup();