<?php

class AW_Blog_Helper_Image extends Mage_Core_Helper_Abstract
{
    public function getResizedUrl($imgUrl,$x=100,$y=100,$color){


		$imgPath=$this->splitImageValue($imgUrl,"path");
		$imgName=$this->splitImageValue($imgUrl,"name");

		/**
		 * Path with Directory Seperator
		 */
		$imgPath=str_replace("/",DS,$imgPath);

		/**
		 * Absolute full path of Image
		 */
		
		 $imgPathFull=Mage::getBaseDir("media").DS.$imgPath.DS.$imgName;
		 

		/**
		 * If Y is not set set it to as X
		 */
		$width=$x;
		$y?$height=$y:$height=$x;

		/**
		 * Resize folder is widthXheight
		 */
		$resizeFolder=$width."X".$height;

		/**
		 * Image resized path will then be
		 */
		 
		$imageResizedPath = Mage::getBaseDir("media").DS.$imgPath.DS.$resizeFolder.DS.$imgName;
		
		/**
		 * First check in cache i.e image resized path
		 * If not in cache then create image of the width=X and height = Y
		 */
		 
		$colorArray = array();
		$colorArray = explode(",",$color);
		if (!file_exists($imageResizedPath) && file_exists($imgPathFull)) :
			
			$imageObj = new Varien_Image($imgPathFull);
			/* $imageObj->constrainOnly(TRUE);
			$imageObj->keepAspectRatio(TRUE);
			$imageObj->keepFrame(TRUE);
			$imageObj->keeptransparency(FALSE);
			$imageObj->backgroundColor(array(intval($colorArray[0]),intval($colorArray[1]),intval($colorArray[2])));
			$imageObj->resize($width, $height);
			$imageObj->save($imageResizedPath); */
			$imageObj->constrainOnly ( true );
			$imageObj->keepAspectRatio ( true );
			$imageObj->keepFrame ( true ); // force Frame
			/* $imageObj->quality ($this->quality); */
			$imageObj->keepTransparency(true);  // keep Transparency with image png
			$imageObj->backgroundColor(array(255,255,255));
			$imageObj->resize ($width, $height);
			$imageObj->save($imageResizedPath);   
		endif;

		/**
		 * Else image is in cache replace the Image Path with / for http path.
		 */
		$imgUrl=str_replace(DS,"/",$imgPath);

		/**
		 * Return full http path of the image
		 */
		return Mage::getBaseUrl("media").$imgUrl."/".$resizeFolder."/".$imgName;
	}
	public function splitImageValue($imageValue,$attr="name"){
		$imArray=explode("/",$imageValue);

		$name=$imArray[count($imArray)-1];
		$path=implode("/",array_diff($imArray,array($name)));
		if($attr=="path"){
			return $path;
		}
		else
			return $name;

	}

}