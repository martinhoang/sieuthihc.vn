<?php
/*------------------------------------------------------------------------
 # SM Market - Version 1.0
 # Copyright (c) 2014 The YouTech Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: YouTech Company
 # Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

class Sm_Market_Model_System_Config_Source_Proshow
{
	public function toOptionArray()
	{	
		$option=array();
		/* if(Mage::getStoreConfig('market_cfg/promotion_set/pro_show')){
			$blog=Mage::getModel('blog/blog')->load(Mage::getStoreConfig('market_cfg/promotion_set/pro_show'));
			$option[]=array('value'=>Mage::getStoreConfig('market_cfg/promotion_set/pro_show'),'label'=>$blog->getTitle());
		}  */
		if(Mage::getStoreConfig('market_cfg/promotion_set/cat_blog')){
		$collection = Mage::getModel('blog/blog')->getCollection()
					->addPresentFilter()
					->addEnableFilter(AW_Blog_Model_Status::STATUS_ENABLED)
					->joinComments()
					->addCatFilter(Mage::getStoreConfig('market_cfg/promotion_set/cat_blog'))
				;
		}
		foreach($collection as $col){
			$option[]=array('value'=>$col->getPostId(),'label'=>$col->getTitle());
		}
		return $option;
	}
}
