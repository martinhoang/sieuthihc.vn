<?php
/*------------------------------------------------------------------------
 # SM Market - Version 1.0
 # Copyright (c) 2014 The YouTech Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: YouTech Company
 # Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

class Sm_Market_Model_System_Config_Source_Catblog
{
	public function toOptionArray()
	{	
		
		$modules = Mage::getConfig()->getNode('modules')->children();
		$modulesArray = (array)$modules;
		$enabled=Mage::getStoreConfig('advanced/modules_disable_output/AW_Blog')==0?true:false;
		$active=Mage::getConfig()->getModuleConfig('AW_Blog')->is('active', 'true');
		if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore()))
		{
			$store_id = Mage::getModel('core/store')->load($code)->getId();
		}
		elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite()))
		{
			$website_id = Mage::getModel('core/website')->load($code)->getId();
			$store_id = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
		}
		else
		{
			$store_id = 0;
		}
		if($enabled&&$active) {
			$option=array();
			$collection = Mage::getModel('blog/cat')
            ->getCollection()
            ->setOrder('sort_order', 'asc');
			if($store_id!=0){
				$collection->addStoreFilter($store_id);
			}
				$option[]=array('value'=>'','label'=>'');
			foreach($collection as $cat){
				$option[]=array('value'=>$cat->getCatId(),'label'=>$cat->getTitle());
				}
				return $option;
		}else {
			return;
		}
	}
}
