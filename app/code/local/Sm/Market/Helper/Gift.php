<?php
/*------------------------------------------------------------------------
 # SM Market - Version 1.0
 # Copyright (c) 2014 The YouTech Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: YouTech Company
 # Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

class Sm_Market_Helper_Gift extends Mage_Core_Helper_Abstract{
	
	public function checkModules(){
		$active=Mage::getConfig()->getModuleConfig('Hchomecenter_Giftplus')->is('active', 'true');
		$enabled=Mage::getStoreConfig('advanced/modules_disable_output/Hchomecenter_Giftplus')==0?true:false;
		if($active&&$enabled){
			return true;
		}
	}
	public function getProductGifts($_product)
	{
		if($this->checkModules()){
			$_collection = array();
			$resource = Mage::getSingleton('core/resource');
			$read = $resource->getConnection('catalog_read');
			$giftProductTable = $resource->getTableName('giftplus_products');
			$select = "select giftplus_id from $giftProductTable where product_id=" . $_product->getId();
			$res = $read->fetchAll($select);
			if (count($res) > 0) {
				foreach ($res as $row) {
					$_gift = Mage::getModel('giftplus/giftplus');
					$_gift->load($row['giftplus_id']);
					if ($_gift->getId() && $_gift->getStatus() == '1' && $_gift->isEnabled()) {
						$_collection[$row['giftplus_id']] = $_gift;
					}
					unset($_gift);
				}
			}
			return $_collection;
		}else{
			return '';
		}
	}
	public function checkGift($_product){
		$_collection=$this->getProductGifts($_product);
		if(count($_collection)>0&&!empty($_collection)){
			return true;
		}
		return false;
	}
	public function getHtmlGift($_product){
		$gifts = $this->getProductGifts($_product);
		$html='';
		if ($_product->getId() && count($gifts) > 0){
			$html.='<div class="product-view-giftplus">
						<div class="product-view-promo">
						<div class="promo-title">';
			$html.=Mage::helper("market")->__("Khuyến Mại : ");
			$html.='</div><ul>';
			foreach($gifts as $gift){
				if ($gift->isEnabled()){
					$html.='<li>';
					$html.=$gift->getContent();
					$html.='</li>';
				}
			}
			$html.='</ul></div></div>';
			return $html;
		}else{
			return '';
		}
		
	}
}