<?php
/*------------------------------------------------------------------------
 # SM Categories - Version 2.0.0
 # Copyright (c) 2014 YouTech Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: YouTech Company
 # Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

class Sm_Market_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

	public function selectAction()
	{
		$html='';
		if($this->getRequest()->getParam('cat_id')){
			 $collection = Mage::getModel('blog/blog')->getCollection()
					->addPresentFilter()
					->addEnableFilter(AW_Blog_Model_Status::STATUS_ENABLED)
					->joinComments()
					->addCatFilter($this->getRequest()->getParam('cat_id'))
				;
			
			foreach($collection as $col)
			{
				$html.='<option value="'.$col->getPostId().'">'.$col->getTitle().'</option>';
			}
			
		}
		$result = array(
				'optionPr'   =>  $html, 
			 );
		$this->getResponse()->setBody(Zend_Json::encode($result));
	}
	protected function _isAllowed()
	{
		return true;
	}
}