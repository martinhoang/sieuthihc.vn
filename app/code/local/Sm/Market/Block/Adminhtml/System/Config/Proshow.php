<?php

?>
<?php
class Sm_Market_Block_Adminhtml_System_Config_Proshow extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element){ 
       	$html = parent::_getElementHtml($element);
		$url=Mage::helper('adminhtml')->getUrl('market/adminhtml_index/select');
		$key=$this->getRequest()->getParam('key');
		$form_key=Mage::getSingleton('core/session')->getFormKey();
		$html.="
		
			<script tyle='text/javascript'>
				 Event.observe('market_cfg_promotion_set_cat_blog', 'change', function(event) {
					var postData=$('market_cfg_promotion_set_cat_blog').getValue();
					new Ajax.Request('".$url."', {
						method: 'post',
						postBody :'form_key=' + FORM_KEY +'&cat_id=' + postData,
						onCreate: function()
						{
						}.bind(this),
						onComplete: function()
						{
							
						}.bind(this),
						onSuccess: function(transport) {
							if (transport.responseText.isJSON()) {
								var response = transport.responseText.evalJSON();
								if (response.error) {
                                    alert(response.error);
                                }else{
									$('".$element->getHtmlId()."').update(response.optionPr);
								}
							}
						}.bind(this),
						onFailure: function()
						{
						
						}.bind(this)    
					});
			});
			</script>
		";
        return $html;
    }
}
?>