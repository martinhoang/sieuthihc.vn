<?php
/*------------------------------------------------------------------------
 # SM Mega Menu - Version 1.1
 # Copyright (c) 2013 YouTech Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: YouTech Company
 # Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

class Sm_Megamenu_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {	
		$this->loadLayout();     
		$this->renderLayout();
    }
	public function getitemsAction(){
		if ($params = Mage::app()->getRequest()->getParams()) {
			if($params['group']){				
				Mage::dispatchEvent('megamenu_menuitems_getItemsByGroupId',array('params'=>$params));
			}
		}
	}
	public function getchilditemsAction(){
		if ($params = Mage::app()->getRequest()->getParams()) {
			if($params['group']){					
				Mage::dispatchEvent('megamenu_menuitems_getChildItemsByParentId',array('params'=>$params));
			}
		}	
	}
	protected function _isAllowed()
	{
		return true;
	}
}