<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('smtag_seo')} ADD  `tag_keywords` text default '';
");
$installer->endSetup();