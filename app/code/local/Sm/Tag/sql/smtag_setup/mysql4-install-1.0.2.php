<?php
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('smtag_seo')};
CREATE TABLE {$this->getTable('smtag_seo')}(
	`id` int(11) unsigned NOT NULL auto_increment,
	`tag_id` int(11) unsigned NOT NULL,
	`tag_title` varchar(255) default '',
	`tag_description` text,
	`store_id` smallint(5) unsigned NOT NULL,
	PRIMARY KEY  (`id`)      
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
");
$installer->endSetup();
