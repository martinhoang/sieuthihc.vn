<?php
class Sm_Tag_Model_Mysql4_Smtag_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('smtag/smtag');
    }
}