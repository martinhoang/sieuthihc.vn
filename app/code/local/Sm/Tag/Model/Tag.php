<?php
class Sm_Tag_Model_Tag extends Mage_Tag_Model_Tag
{
    public function getTaggedProductsUrl()
    {
		return Mage::getUrl('', array('_direct' => 'tag/'.str_replace(" ", "-", $this->getName())));
    }
}
