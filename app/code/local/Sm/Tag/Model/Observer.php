<?php
class Sm_Tag_Model_Observer
{
	  
     public function BlockOutput($observer) 
    {
        $_block = $observer->getBlock();
        $_type = $_block->getType();
        /* $transport = $observer->getTransport();
        $html = $transport->getHtml(); */
		
        if ($_block instanceof Mage_Tag_Block_Product_Result) {  
			$storeId = Mage::app()->getStore()->getStoreId();
            $tagId=Mage::app()->getRequest()->getParam('tagId');
			$tagModelSeo=Mage::getModel('smtag/smtag')->getCollection()
													->addFieldToFilter('tag_id',$tagId)
													->addFieldToFilter('store_id',$storeId)
													->getFirstItem()
													->getData();
			$_block->getLayout()->getBlock('head')->setTitle($tagModelSeo['tag_title']);
			$_block->getLayout()->getBlock('head')->setDescription($tagModelSeo['tag_description']);
			$_block->getLayout()->getBlock('head')->setKeywords($tagModelSeo['tag_keywords']);
        }
	}

}