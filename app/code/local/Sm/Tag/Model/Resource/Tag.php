<?php
class Sm_Tag_Model_Resource_Tag extends Mage_Tag_Model_Resource_Tag
{
    public function loadByName($model, $name)
    {
        parent::loadByName($model, $name);
        if ($model->getId()) {
            $this->_afterLoad($model);
        } else {
            return false;
        }
    }
	protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
		$modelTag = Mage::getModel('tag/tag');
		$TaGCollection= $modelTag->getResourceCollection()
                ->addPopularity()
                /* ->addStatusFilter($model->getApprovedStatus()) */
                /* ->addProductFilter($ProductId) */
                /* ->setFlag('relation', true) */
                /* ->addStoreFilter(Mage::app()->getStore()->getId()) */
                /* ->setActiveFilter() */
                ->load();
		$tagIdsArray=array();
		$tagIdsSeoArray=array();
		$Tags=$TaGCollection->getItems();
		if(count($Tags)){
			 foreach ($Tags as $tag) {
				$tagIdsArray[]=$tag->getTagId();
			 }
		}
		
		$modelTagSeo=Mage::getModel('smtag/smtag')->getCollection();
		foreach($modelTagSeo as $TagSeo){
			$tagIdsSeoArray[]=$TagSeo->getTagId();
		}
			
		$tagIdsSeoArray=array_unique($tagIdsSeoArray);
	
		$TagIdsDelete=array_diff ($tagIdsSeoArray, $tagIdsArray);
		$condition= $this->_getWriteAdapter()->quoteInto('tag_id IN (?)', $TagIdsDelete);
		/* $this->_getWriteAdapter()->delete($this->getTable('smtag/smtag'), $condition); */
		$read = $this->_getReadAdapter();
        /* $select = $read->select()
            ->from($this->getTable('tag/summary'), array('store_id'))
           ->where('tag_id = ?', $object->getId()); */
        /* $storeIds = $read->fetchCol($select); */
		$storeId=Mage::app()->getRequest()->getParam('store');
		$select_sm = $this->_getReadAdapter()->select('tag_title','tag_description')
            ->from($this->getTable('smtag/smtag'))
            ->where('tag_id = ?',$object->getId())
			/* ->where('store_id IN (?)',$storeIds); */
			->where('store_id = ?',$storeId);
		if ($data = $this->_getReadAdapter()->fetchRow($select_sm)) {
            $tag_title= $data['tag_title'];
			$tag_description= $data['tag_description'];
			$tag_keywords=$data['tag_keywords'];
            $object->setData('tag_title', $tag_title);
			$object->setData('tag_description', $tag_description);
			$object->setData('tag_keywords', $tag_keywords);
		}
        return parent::_afterLoad($object);
    }
	 protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
		
        $adapter  = $this->_getWriteAdapter();
        $tagId = ($object->isObjectNew()) ? $object->getTagId() : $object->getId();
		/* $storeId=Mage::app()->getStore()->getId(); */
		$storeId=Mage::app()->getRequest()->getParam('store');
		$condition[]= $this->_getWriteAdapter()->quoteInto('tag_id = ?', $tagId);
		$condition[]=$this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId);
		$this->_getWriteAdapter()->delete($this->getTable('smtag/smtag'), $condition);
		$tag_data=array(
				'tag_id'=>$tagId,
				'tag_title'=>$_POST['tag_title'],
				'tag_description'=>$_POST['tag_description'],
				'store_id'	=>$object->getStore(),
				'tag_keywords'=>$_POST['tag_keywords'],
		);
		$this->_getWriteAdapter()->insert($this->getTable('smtag/smtag'), $tag_data);    
        return parent::_afterSave($object);
    }
}
