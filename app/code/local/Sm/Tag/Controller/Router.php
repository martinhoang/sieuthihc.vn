<?php
class Sm_Tag_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    public function initControllerRouters($observer)
    {
        $front = $observer->getEvent()->getFront();
        $front->addRouter('sm_tag', $this);
        return $this;
    }

    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }
        $urlKey = trim($request->getPathInfo(), '/');
        $parts = explode('/', $urlKey);
        if (count($parts) != 2) {
            return false;
        }
        if ($parts[0] != 'tag') {
            return false;
        }
		$tagName = str_replace("-", " ", $parts[1]);
        $tag = Mage::getModel('tag/tag')->loadByName($tagName);
        if(!$tag->getId() || !$tag->isAvailableInStore()) {
            return false;
        }
        $request->setModuleName('tag')
            ->setControllerName('product')
            ->setActionName('list')
            ->setParam('tagId', $tag->getId());
        $request->setAlias(
            Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
            $urlKey
        );
        return true;
    }
}
