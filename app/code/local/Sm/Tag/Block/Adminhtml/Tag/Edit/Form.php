<?php

class Sm_Tag_Block_Adminhtml_Tag_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('tag_form');
        $this->setTitle(Mage::helper('tag')->__('Block Information'));
    }

     protected function _prepareForm()
    {
        $model = Mage::registry('tag_tag');

        $form = new Varien_Data_Form(
            array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post')
        );

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend'=>Mage::helper('tag')->__('General Information')));

        if ($model->getTagId()) {
            $fieldset->addField('tag_id', 'hidden', array(
                'name' => 'tag_id',
            ));
        }

        $fieldset->addField('form_key', 'hidden', array(
            'name'  => 'form_key',
            'value' => Mage::getSingleton('core/session')->getFormKey(),
        ));

        $fieldset->addField('store_id', 'hidden', array(
            'name'  => 'store_id',
            'value' => (int)$this->getRequest()->getParam('store')
        ));

        $fieldset->addField('name', 'text', array(
            'name' => 'tag_name',
            'label' => Mage::helper('tag')->__('Tag Name'),
            'title' => Mage::helper('tag')->__('Tag Name'),
            'required' => true,
            'after_element_html' => ' ' . Mage::helper('adminhtml')->__('[GLOBAL]'),
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('tag')->__('Status'),
            'title' => Mage::helper('tag')->__('Status'),
            'name' => 'tag_status',
            'required' => true,
            'options' => array(
                Mage_Tag_Model_Tag::STATUS_DISABLED => Mage::helper('tag')->__('Disabled'),
                Mage_Tag_Model_Tag::STATUS_PENDING  => Mage::helper('tag')->__('Pending'),
                Mage_Tag_Model_Tag::STATUS_APPROVED => Mage::helper('tag')->__('Approved'),
            ),
            'after_element_html' => ' ' . Mage::helper('adminhtml')->__('[GLOBAL]'),
        ));

        $fieldset->addField('base_popularity', 'text', array(
            'name' => 'base_popularity',
            'label' => Mage::helper('tag')->__('Base Popularity'),
            'title' => Mage::helper('tag')->__('Base Popularity'),
            'after_element_html' => ' ' . Mage::helper('tag')->__('[STORE VIEW]'),
        ));
		  $fieldset->addField('tag_title', 'text', array(
            'name' => 'tag_title',
            'label' => Mage::helper('tag')->__('Title'),
            'title' => Mage::helper('tag')->__('Title'),
            'after_element_html' => ' ' . Mage::helper('tag')->__('[STORE VIEW]'),
        ));
		  $fieldset->addField('tag_description', 'textarea', array(
            'name' => 'tag_description',
            'label' => Mage::helper('tag')->__('Description'),
            'title' => Mage::helper('tag')->__('Description'),
            'after_element_html' => ' ' . Mage::helper('tag')->__('[STORE VIEW]'),
        ));
		 $fieldset->addField('tag_keywords', 'textarea', array(
            'name' => 'tag_keywords',
            'label' => Mage::helper('tag')->__('Keywords'),
            'title' => Mage::helper('tag')->__('Keywords'),
            'after_element_html' => ' ' . Mage::helper('tag')->__('[STORE VIEW]'),
        ));
        if (!$model->getId() && !Mage::getSingleton('adminhtml/session')->getTagData() ) {
            $model->setStatus(Mage_Tag_Model_Tag::STATUS_APPROVED);
        }

        if ( Mage::getSingleton('adminhtml/session')->getTagData() ) {
            $form->addValues(Mage::getSingleton('adminhtml/session')->getTagData());
            Mage::getSingleton('adminhtml/session')->setTagData(null);
        } else {
            $form->addValues($model->getData());
        }

        $this->setForm($form);
        return parent::_prepareForm();
    }
    /* protected function _setTagCodes($tagCodes)
    {
        foreach($tagCodes as $store=>$value) {
            if ($element = $this->getForm()->getElement('tag_code_' . $store)) {
               $element->setValue($value);
            }
        }
    } */
    
    protected function _toHtml()
    {
        return $this->_getWarningHtml() . parent::_toHtml();
    }

    protected function _getWarningHtml()
    {
        return '<div>
        <ul class="messages">
            <li class="notice-msg">
                <ul>
                    <li>'.$this->__('If you do not specify a tag title for a store, the default value will be used.').'</li>
                </ul>
            </li>
        </ul>
        </div>';
    }
}
