<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Material
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Material Helper
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Material
 * @author      Hchomecenter Developer
 */
class Hchomecenter_Material_Helper_Data extends Mage_Core_Helper_Abstract
{
    
}