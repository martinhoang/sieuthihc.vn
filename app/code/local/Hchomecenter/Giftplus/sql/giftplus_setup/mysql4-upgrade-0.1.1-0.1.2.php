<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('giftplus')} ADD  `custom_status` smallint(6) NOT NULL default '0';
");
$installer->endSetup();