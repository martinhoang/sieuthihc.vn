<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('giftplus')} ADD  `status_text` smallint(6) NOT NULL default '1';
");
$installer->endSetup();