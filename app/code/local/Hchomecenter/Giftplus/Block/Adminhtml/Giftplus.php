<?php
class Hchomecenter_Giftplus_Block_Adminhtml_Giftplus extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_giftplus';
    $this->_blockGroup = 'giftplus';
    $this->_headerText = Mage::helper('giftplus')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('giftplus')->__('Add Item');
    parent::__construct();
  }
}