<?php

class Hchomecenter_Giftplus_Block_Adminhtml_Giftplus_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('giftplus_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('giftplus')->__('Item Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('giftplus')->__('Gift Information'),
            'title' => Mage::helper('giftplus')->__('Gift Information'),
            'content' => $this->getLayout()->createBlock('giftplus/adminhtml_giftplus_edit_tab_form')->toHtml(),
        ));

        $this->addTab('product_section', array(
            'label' => Mage::helper('giftplus')->__('Apply To Products'),
            'title' => Mage::helper('giftplus')->__('Apply To Products'),
            'content' => $this->getLayout()->createBlock('giftplus/adminhtml_giftplus_edit_tab_product')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}