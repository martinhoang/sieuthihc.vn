<?php

class Hchomecenter_Giftplus_Block_Adminhtml_Giftplus_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('giftplus_form', array('legend'=>Mage::helper('giftplus')->__('Item information')));
	
	$fieldset->addField('title', 'text', array(
	  'label'     => Mage::helper('giftplus')->__('Title'),
	  'class'     => 'required-entry',
	  'required'  => true,
	  'name'      => 'title',
	));
	
	$fieldset->addField('filename', 'file', array(
	  'label'     => Mage::helper('giftplus')->__('File'),
	  'required'  => false,
	  'name'      => 'filename',
	));
	
	$fieldset->addField('status', 'select', array(
	  'label'     => Mage::helper('giftplus')->__('Status'),
	  'name'      => 'status',
	  'values'    => array(
		  array(
			  'value'     => 1,
			  'label'     => Mage::helper('giftplus')->__('Enabled'),
		  ),
	
		  array(
			  'value'     => 2,
			  'label'     => Mage::helper('giftplus')->__('Disabled'),
		  ),
	  ),
	));
	$fieldset->addField('status_text', 'select', array(
	  'label'     => Mage::helper('giftplus')->__('Hiển thị text chuyển động'),
	  'name'      => 'status_text',
	  'values'    => array(
		  array(
			  'value'     => 1,
			  'label'     => Mage::helper('giftplus')->__('Enabled'),
		  ),
	
		  array(
			  'value'     => 2,
			  'label'     => Mage::helper('giftplus')->__('Disabled'),
		  ),
	  ),
	));
	
	$fieldset->addField('content', 'editor', array(
	  'name'      => 'content',
	  'label'     => Mage::helper('giftplus')->__('Content'),
	  'title'     => Mage::helper('giftplus')->__('Content'),
	  'style'     => 'width:700px; height:500px;',
	  'wysiwyg'   => false,
	  'required'  => true,
	));
	  
	$fieldset->addField('start_day', 'date', array(
		'name'      => 'start_day',
		'label'     => Mage::helper('giftplus')->__('Start Day'),
		'image'     => $this->getSkinUrl('images/grid-cal.gif'),
		'format'    => 'yyyy-M-d',
		'class'     => 'required-entry',
		'required'  => true,
	));
	
	$fieldset->addField('expired_day', 'date', array(
		'name'      => 'expired_day',
		'label'     => Mage::helper('giftplus')->__('Expired Day'),
		'image'     => $this->getSkinUrl('images/grid-cal.gif'),
		'format'    => 'yyyy-M-d',
		'class'     => 'required-entry',
		'required'  => true,
	));
     
	if ( Mage::getSingleton('adminhtml/session')->getGiftplusData() )
	{
	  $form->setValues(Mage::getSingleton('adminhtml/session')->getGiftplusData());
	  Mage::getSingleton('adminhtml/session')->setGiftplusData(null);
	} elseif ( Mage::registry('giftplus_data') ) {
	  $form->setValues(Mage::registry('giftplus_data')->getData());
	}
	return parent::_prepareForm();
  }
}