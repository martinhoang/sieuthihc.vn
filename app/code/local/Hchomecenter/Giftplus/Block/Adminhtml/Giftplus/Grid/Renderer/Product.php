<?php

class Hchomecenter_Giftplus_Block_Adminhtml_Giftplus_Grid_Renderer_Product extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	protected $_values;

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
		$id = $row->getData($this->getColumn()->getIndex());
		$values = $row->getData($this->getColumn()->getValues());
		print_r ($values);
        $html = '<input type="checkbox" name="selected_products[]" value="'.$id.'"/>';
        return $html;
    }	  
}