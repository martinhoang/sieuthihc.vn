<?php

class Hchomecenter_Giftplus_Block_Giftplus extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getGiftplus() {
        if (!$this->hasData('giftplus')) {
            $this->setData('giftplus', Mage::registry('giftplus'));
        }
        return $this->getData('giftplus');
    }

    public function getProductGifts($_product) {
        $_collection = array();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('catalog_read');
        $giftProductTable = $resource->getTableName('giftplus_products');
        $select = "select giftplus_id from $giftProductTable where product_id=" . $_product->getId();
        $res = $read->fetchAll($select);
        if (count($res) > 0) {
            foreach ($res as $row) {
                $_gift = Mage::getModel('giftplus/giftplus');
                $_gift->load($row['giftplus_id']);
                if ($_gift->getId() && $_gift->getStatus() == '1' && $_gift->isEnabled()) {
                    $_collection[$row['giftplus_id']] = $_gift;
                }
                unset($_gift);
            }
        }
        return $_collection;
    }

}