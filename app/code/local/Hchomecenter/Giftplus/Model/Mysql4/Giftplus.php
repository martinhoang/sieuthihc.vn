<?php

class Hchomecenter_Giftplus_Model_Mysql4_Giftplus extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the giftplus_id refers to the key field in your database table.
        $this->_init('giftplus/giftplus', 'giftplus_id');
    }
}