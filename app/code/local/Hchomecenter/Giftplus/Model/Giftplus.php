<?php

class Hchomecenter_Giftplus_Model_Giftplus extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('giftplus/giftplus');
    }

    public function getProducts() {
        if (!$this->getId()) {
            return array();
        }
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('catalog_read');
        $giftProductTable = $resource->getTableName('giftplus_products');
        $selectQuery = "select product_id from $giftProductTable where giftplus_id = " . $this->getId();
        $res = $read->fetchAll($selectQuery);
        $collection = array();
        if (count($res) > 0) {
            foreach ($res as $row) {
                $collection[$row['product_id']] = $row['product_id'];
            }
        }
        return $collection;
    }

    public function getProductSelected() {
        $productIds = array();
        if (Mage::getSingleton('core/session')->getGiftProductIds()) {
            $productIds = Mage::getSingleton('core/session')->getGiftProductIds();
        }
        return $productIds;
    }

    public function getPlus() {
        if (!$this->getId()) {
            return array();
        }
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('catalog_read');
        $giftProductTable = $resource->getTableName('giftplus_plus');
        $selectQuery = "select product_id from $giftProductTable where giftplus_id = " . $this->getId();
        $res = $read->fetchAll($selectQuery);
        $collection = array();
        if (count($res) > 0) {
            foreach ($res as $row) {
                $collection[$row['product_id']] = $row['product_id'];
            }
        }
        return $collection;
    }

    public function getPlusSelected() {
        $productIds = array();
        if (Mage::getSingleton('core/session')->getPlusProductIds()) {
            $productIds = Mage::getSingleton('core/session')->getPlusProductIds();
        }
        return $productIds;
    }

    public function isEnabled() {
        if (!$this->getId()) {
            return false;
        }
        if ($this->getStatus() != '1') {
            return false;
        }
        $day = gmdate("d", time() + 3600 * (7 + date("0")));
        $month = gmdate("m", time() + 3600 * (7 + date("0")));
        $year = gmdate("Y", time() + 3600 * (7 + date("0")));
        $startDate = str_replace('-', '', substr($this->getStartDay(), 0, 10));
        $expDate = str_replace('-', '', substr($this->getExpiredDay(), 0, 10));
        $today = $year . $month . $day;
        //echo 'Từ: ' . $day . '/' . $month . '/' . $year;
        if (intval($startDate) <= intval($today) && intval($today) <= intval($expDate)) {
            return true;
        }
        return false;
    }

}