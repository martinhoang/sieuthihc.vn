<?php 
	class Hchomecenter_Giftplus_Model_Observer
	{
		public function deleteOverdueGiftPus()
		{
			$today = Mage::getModel('core/date')->date('Y-m-d H:i:s');
			$resource = Mage::getSingleton('core/resource');
			$read = $resource->getConnection('catalog_read');
			$write = $resource->getConnection('catalog_write');
			$giftProductTable = $resource->getTableName('giftplus_products');
			$giftplusTable = $resource->getTableName('giftplus');
			$query = "SELECT `giftplus_id` FROM `giftplus` WHERE `expired_day` < '$today' ";
			$res = $read->fetchAll($query);
			foreach($res as $re){
				$bindQuery = "delete from $giftProductTable where giftplus_id = " .(int)$re['giftplus_id'];
				$write->query($bindQuery);
				$giftplus = Mage::getModel('giftplus/giftplus')->load((int)$re['giftplus_id']);
				$giftplus->delete();
			}
		}
	}					