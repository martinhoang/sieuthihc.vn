<?php
	class Hchomecenter_Giftplus_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action
	{
		protected function _initAction()
		{
			$this->loadLayout()
			->_setActiveMenu('giftplus/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Giftplus CSV'), Mage::helper('adminhtml')->__('Import Giftplus CSV'));
			
			return $this;
		}   
		
		public function indexAction() 
		{
			$this->_initAction()
			->renderLayout();
		}
		
		public function importCsvAction() 
		{
			if($_FILES['giftplus_csv']['name'] != '') {
				$data = $this->getRequest()->getPost();
				if($data){
					try {	
						$uploader = new Varien_File_Uploader('giftplus_csv');
						$uploader->setAllowedExtensions(array('csv'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$path = Mage::getBaseDir('media') . DS.'hc'.DS.'giftplus'.DS.'import'.DS;
						$uploader->save($path, $_FILES['giftplus_csv']['name'] );
						$csv = new Varien_File_Csv();
						$content = $csv->getData($path.$_FILES['giftplus_csv']['name']);
						for ($i=0; $i<count($content) ; $i++) {
							$productId = Mage::getModel('catalog/product')->getResource()->getIdBySku('HC'.$content[$i][0]);
							if($productId){
								// Change code to remove previous gift's product
								// if you want to nest gift that remove this code
								//and edit template giftplus/product/block.phtml
								$resource = Mage::getSingleton('core/resource');
								$read = $resource->getConnection('catalog_read');
								$write = $resource->getConnection('catalog_write');
								$giftProductTable = $resource->getTableName('giftplus_products');
								$giftplusTable = $resource->getTableName('giftplus');
								$query = "SELECT `giftplus_id` FROM $giftProductTable WHERE `product_id` = " . $productId;
								$res = $read->fetchAll($query);
								foreach($res as $re){
									$bindQuery = "delete from $giftProductTable where giftplus_id = " .(int)$re['giftplus_id'];
									$write->query($bindQuery);
									$giftplus = Mage::getModel('giftplus/giftplus')->load((int)$re['giftplus_id']);
									$giftplus->delete();
								}
								// End additon code
								$start_day = Mage::app()->getLocale()->date(
								$content[$i][2],
								Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
								null, 
								false
								);
								$expired_day = Mage::app()->getLocale()->date(
								$content[$i][3],
								Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
								null, 
								false
								);
								$text = '';
								if($content[$i][4]!='  ' && $content[$i][4]!=''){
									$text.='- Tặng công lắp đặt'.'<br/>';
								}
								if($content[$i][6]!='  '&& $content[$i][6]!=''){
									$text.='- Tặng phiếu mua hàng trị giá '.$content[$i][6].'đ'.'<br/>';
								}
								if($content[$i][7]!='  ' && $content[$i][7]!=''){
									$text.='- Tặng '.$content[$i][7].'<br/>';
								}
								if($content[$i][10]!='  ' && $content[$i][10]!=''){
									$text.='- Tặng '.$content[$i][10].'<br/>';
								}
								if($content[$i][13]!='  ' && $content[$i][13]!=''){
									$text.='- Tặng '.$content[$i][13];
								}
								$model = Mage::getModel('giftplus/giftplus')
								->setTitle($content[$i][1])
								->setStatus('1')
								->setContent($text)
								->setStartDay($start_day)
								->setExpiredDay($expired_day);
								if($text==''){
									$model->setStatusText('1');
								}
								else{
									$model->setStatusText('2');
								}
								$model->save();
								$resource = Mage::getSingleton('core/resource');
								$write = $resource->getConnection('catalog_write');
								$giftProductTable = $resource->getTableName('giftplus_products');
								$id = $model->getId();
								$bindQuery = "insert into $giftProductTable(giftplus_id, product_id) values ($id, $productId)";
								$write->query($bindQuery);
							}
						}
						Mage::getSingleton('core/session')->addSuccess('Import thành công.');
					}
					catch (Exception $e) {
						Mage::getModel('core/session')->addError(Mage::helper('giftplus')->__('Kiểu file không hợp lệ, hoặc sai mã SKU!'));
					}
					$this->_redirect('*/*/');
				}
			}
			else {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giftplus')->__('Không thể tìm thấy các file để import'));
				$this->_redirect('*/*/');
			}
		}
		protected function _isAllowed()
		{
			return true;
		}
	}																														