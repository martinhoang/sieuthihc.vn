<?php
	
	class Hchomecenter_Giftplus_Adminhtml_GiftplusController extends Mage_Adminhtml_Controller_Action {
		
		protected function _initAction() {
			$this->loadLayout()
			->_setActiveMenu('giftplus/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			
			return $this;
		}
		
		public function gridAction() {
			$this->loadLayout();
			$this->getLayout()->getBlock('gift.products')
			->setSelectedProducts($this->getRequest()->getPost('selected_products', null));
			$this->renderLayout();
		}
		
		public function plusAction() {
			$this->loadLayout();
			$this->getLayout()->getBlock('gift.plus')
			->setSelectedProducts($this->getRequest()->getPost('selected_products', null));
			$this->renderLayout();
		}
		
		public function indexAction() {
			$this->_initAction()
			->renderLayout();
		}
		
		public function editAction() {
			$id = $this->getRequest()->getParam('id');
			$model = Mage::getModel('giftplus/giftplus')->load($id);
			
			if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}
				
				Mage::register('giftplus_data', $model);
				
				$this->loadLayout();
				$this->_setActiveMenu('giftplus/items');
				
				$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
				$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
				
				$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('giftplus/adminhtml_giftplus_edit'))
				->_addLeft($this->getLayout()->createBlock('giftplus/adminhtml_giftplus_edit_tabs'));
				
				$this->renderLayout();
				} else {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giftplus')->__('Item does not exist'));
				$this->_redirect('*/*/');
			}
		}
		
		public function newAction() {
			Mage::getSingleton('core/session')->setPlusProductIds(null);
			Mage::getSingleton('core/session')->setGiftProductIds(null);
			$this->_forward('edit');
		}
		
		public function saveAction() {
			if ($data = $this->getRequest()->getPost()) {
				
				if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
					try {
						/* Starting upload */
						$uploader = new Varien_File_Uploader('filename');
						
						// Any extention would work
						$uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						
						// We set media as the upload dir
						$path = Mage::getBaseDir('media') . DS;
						$uploader->save($path, $_FILES['filename']['name']);
					}
					catch (Exception $e) {	
					}
					//this way the name is saved in DB
					$data['filename'] = $_FILES['filename']['name'];
				}
				
				
				$model = Mage::getModel('giftplus/giftplus');
				$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
				
				try {
					if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
						$model->setCreatedTime(now())
						->setUpdateTime(now());
					} 
					else {
						$model->setUpdateTime(now());
					}
					
					$model->save();
					$resource = Mage::getSingleton('core/resource');
					$write = $resource->getConnection('catalog_write');
					$giftProductTable = $resource->getTableName('giftplus_products');
					$productIds = Mage::getSingleton('core/session')->getGiftProductIds();
					if (count($productIds) > 0) {
						$id = $this->getRequest()->getParam('id');
						if (!$id) {
							$id = $model->getId();
							$bindQuery = "delete from $giftProductTable where giftplus_id = " . $id;
							$write->query($bindQuery);
							foreach ($productIds as $productId) {
								$bindQuery = "insert into $giftProductTable(giftplus_id, product_id) values ($id, $productId)";
								$write->query($bindQuery);
							}
						}
					}
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('giftplus')->__('Item was successfully saved'));
					Mage::getSingleton('adminhtml/session')->setFormData(false);
					
					if ($this->getRequest()->getParam('back')) {
						$this->_redirect('*/*/edit', array('id' => $model->getId()));
						return;
					}
					$this->_redirect('*/*/');
					return;
				}
				catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giftplus')->__('Unable to find item to save'));
			$this->_redirect('*/*/');
		}
		
		public function deleteAction() {
			if ($this->getRequest()->getParam('id') > 0) {
				try {
					$model = Mage::getModel('giftplus/giftplus');
					
					$model->setId($this->getRequest()->getParam('id'))
					->delete();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
					$this->_redirect('*/*/');
					} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				}
			}
			$this->_redirect('*/*/');
		}
		
		public function massDeleteAction() {
			$giftplusIds = $this->getRequest()->getParam('giftplus');
			if (!is_array($giftplusIds)) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
				} else {
				try {
					foreach ($giftplusIds as $giftplusId) {
						$resource = Mage::getSingleton('core/resource');
						$write = $resource->getConnection('catalog_write');
						$giftProductTable = $resource->getTableName('giftplus_products');
						$bindQuery = "delete from $giftProductTable where giftplus_id = " . $giftplusId;
						$write->query($bindQuery);
						$giftplus = Mage::getModel('giftplus/giftplus')->load($giftplusId);
						$giftplus->delete();
					}
					Mage::getSingleton('adminhtml/session')->addSuccess(
					Mage::helper('adminhtml')->__(
					'Total of %d record(s) were successfully deleted', count($giftplusIds)
					)
					);
					} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				}
			}
			$this->_redirect('*/*/index');
		}
		
		public function massStatusAction() {
			$giftplusIds = $this->getRequest()->getParam('giftplus');
			if (!is_array($giftplusIds)) {
				Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
				} else {
				try {
					foreach ($giftplusIds as $giftplusId) {
						$giftplus = Mage::getSingleton('giftplus/giftplus')
						->load($giftplusId)
						->setStatus($this->getRequest()->getParam('status'))
						->setIsMassupdate(true)
						->save();
					}
					$this->_getSession()->addSuccess(
					$this->__('Total of %d record(s) were successfully updated', count($giftplusIds))
					);
					} catch (Exception $e) {
					$this->_getSession()->addError($e->getMessage());
				}
			}
			$this->_redirect('*/*/index');
		}
		
		public function exportCsvAction() {
			$fileName = 'giftplus.csv';
			$content = $this->getLayout()->createBlock('giftplus/adminhtml_giftplus_grid')
			->getCsv();
			
			$this->_sendUploadResponse($fileName, $content);
		}
		
		public function exportXmlAction() {
			$fileName = 'giftplus.xml';
			$content = $this->getLayout()->createBlock('giftplus/adminhtml_giftplus_grid')
			->getXml();
			
			$this->_sendUploadResponse($fileName, $content);
		}
		
		protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream') {
			$response = $this->getResponse();
			$response->setHeader('HTTP/1.1 200 OK', '');
			$response->setHeader('Pragma', 'public', true);
			$response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
			$response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
			$response->setHeader('Last-Modified', date('r'));
			$response->setHeader('Accept-Ranges', 'bytes');
			$response->setHeader('Content-Length', strlen($content));
			$response->setHeader('Content-type', $contentType);
			$response->setBody($content);
			$response->sendResponse();
			die;
		}
		protected function _isAllowed()
		{
			return true;
		}
	}																												