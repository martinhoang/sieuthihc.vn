<?php

class Hchomecenter_Giftplus_AjaxController extends Mage_Adminhtml_Controller_Action {

    public function bindAction() {
        try {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('catalog_write');

            $giftProductTable = $resource->getTableName('giftplus_products');

            $giftId = $this->getRequest()->getParam('giftId');
            $productId = $this->getRequest()->getParam('productId');
            if ($giftId != 0) {
                $bindQuery = "insert into $giftProductTable(giftplus_id, product_id) values ($giftId, $productId)";
                $write->query($bindQuery);
            } else {
                // Add to session data
                if (!Mage::getSingleton('core/session')->getGiftProductIds()) {
                    $productIds = array();
                } else {
                    $productIds = Mage::getSingleton('core/session')->getGiftProductIds();
                }
                $productIds[$productId] = $productId;
                Mage::getSingleton('core/session')->setGiftProductIds($productIds);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function unbindAction() {
        try {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('catalog_write');

            $giftProductTable = $resource->getTableName('giftplus_products');

            $giftId = $this->getRequest()->getParam('giftId');
            $productId = $this->getRequest()->getParam('productId');
            if ($giftId != 0) {
                $unbindQuery = "delete from $giftProductTable where product_id = $productId and giftplus_id = $giftId";
                $write->query($unbindQuery);
            } else {
                // Add to session data
                if (!Mage::getSingleton('core/session')->getGiftProductIds()) {
                    $productIds = array();
                } else {
                    $productIds = Mage::getSingleton('core/session')->getGiftProductIds();
                }
                unset($productIds[$productId]);
                Mage::getSingleton('core/session')->setGiftProductIds($productIds);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function plusAction() {
        try {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('catalog_write');

            $giftProductTable = $resource->getTableName('giftplus_plus');

            $giftId = $this->getRequest()->getParam('giftId');
            $productId = $this->getRequest()->getParam('productId');
            if ($giftId != 0) {
                $bindQuery = "insert into $giftProductTable(giftplus_id, product_id) values ($giftId, $productId)";
                $write->query($bindQuery);
            } else {
                // Add to session data
                if (!Mage::getSingleton('core/session')->getPlusProductIds()) {
                    $productIds = array();
                } else {
                    $productIds = Mage::getSingleton('core/session')->getPlusProductIds();
                }
                $productIds[$productId] = $productId;
                Mage::getSingleton('core/session')->setPlusProductIds($productIds);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function unplusAction() {
        try {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('catalog_write');

            $giftProductTable = $resource->getTableName('giftplus_plus');

            $giftId = $this->getRequest()->getParam('giftId');
            $productId = $this->getRequest()->getParam('productId');
            if ($giftId != 0) {
                $unbindQuery = "delete from $giftProductTable where product_id = $productId and giftplus_id = $giftId";
                $write->query($unbindQuery);
            } else {
                // Add to session data
                if (!Mage::getSingleton('core/session')->getPlusProductIds()) {
                    $productIds = array();
                } else {
                    $productIds = Mage::getSingleton('core/session')->getPlusProductIds();
                }
                unset($productIds[$productId]);
                Mage::getSingleton('core/session')->setPlusProductIds($productIds);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

	protected function _isAllowed()
	{
		return true;
	}
}