<?php
	/**
		* Magestore
		* 
		* NOTICE OF LICENSE
		* 
		* This source file is subject to the Magestore.com license that is
		* available through the world-wide-web at this URL:
		* http://www.magestore.com/license-agreement.html
		* 
		* DISCLAIMER
		* 
		* Do not edit or add to this file if you wish to upgrade this extension to newer
		* version in the future.
		* 
		* @category    Magestore
		* @package     Magestore_Installment
		* @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
		* @license     http://www.magestore.com/license-agreement.html
	*/
	
	/**
		* Installment Index Controller
		* 
		* @category    Magestore
		* @package     Magestore_Installment
		* @author      Magestore Developer
	*/
	class Hchomecenter_Installment_IndexController extends Mage_Core_Controller_Front_Action
	{
		/**
			* index action
		*/
		public function indexAction()
		{
			$this->loadLayout();
			$this->renderLayout();
		}
		public function saveAction(){
			$session = Mage::getSingleton('core/session');
			$data = $this->getRequest()->getPost();
			$price = $data['price'];
			$result = $price * $data['installment_value']/100;
			$pre = number_format($result, 2) . 'đ';
			$installment_info = " Giấy tờ có: ".$data['paper_type']." + "
			." % số tiền trả trước: ".$data['installment_value']."% :".$pre." + "
			." Số tháng vay: ".$data['installment_period']." tháng"." + "
			." Số tiền trả hàng tháng: ".$data['monthly']." + "
			." Email đăng ký: ".$data['email'];
			if ($data) {
				$model = Mage::getModel('installment/installment');
				$model->setProductName($data['product_name'])
				->setSku($data['sku'])
				->setCustomerName($data['customer_name'])
				->setMobile($data['mobile'])
				->setPartner($data['installment_partner'])
				->setInstallmentInfo($installment_info)
				->setCreatedAt(now());
				$model->save();
				$session->addSuccess($this->__('Bạn đã gửi đăng ký thành công'));
				if(isset($model)){
					$recipientName = Mage::getStoreConfig('installment/general/recipient_name', $store);
					$recipientEmail = Mage::getStoreConfig('installment/general/recipient_email', $store);
					$store = Mage::app()->getStore();
					$translate = Mage::getSingleton('core/translate');
					$translate->setTranslateInline(false);
					
					Mage::getModel('core/email_template')
					->addBcc($data['email'])
					->setDesignConfig(array(
					'area' => 'frontend',
					'store' => $store->getId()
					))->sendTransactional(
					Mage::getStoreConfig('installment/general/email', $store),
					Mage::getStoreConfig('trans_email/ident_general', $store),
					$recipientEmail,
					$recipientName,
					array(
					'store' => $store,
					'recipient_name' => $recipientName,
					'recipient_email' => $recipientEmail,
					'customer_name' => $data['customer_name'],
					'product_name' => $data['product_name'],
					'sku' => $data['sku'],
					'mobile' => $data['mobile'],
					'partner' => $data['installment_partner'],
					'installment_info' => $installment_info
					)
					);
					$translate->setTranslateInline(true);
					$this->loadLayout();
					$this->renderLayout();
				}
			}	
		}
	}											