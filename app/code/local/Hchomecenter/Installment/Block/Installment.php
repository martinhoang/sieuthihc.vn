<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Installment Block
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @author      Magestore Developer
 */
class Hchomecenter_Installment_Block_Installment extends Mage_Core_Block_Template
{
    /**
     * prepare block's layout
     *
     * @return Hchomecenter_Installment_Block_Installment
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}