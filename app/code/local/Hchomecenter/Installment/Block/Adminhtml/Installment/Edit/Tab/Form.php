<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Installment Edit Form Content Tab Block
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @author      Magestore Developer
 */
class Hchomecenter_Installment_Block_Adminhtml_Installment_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Hchomecenter_Installment_Block_Adminhtml_Installment_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        if (Mage::getSingleton('adminhtml/session')->getInstallmentData()) {
            $data = Mage::getSingleton('adminhtml/session')->getInstallmentData();
            Mage::getSingleton('adminhtml/session')->setInstallmentData(null);
        } elseif (Mage::registry('installment_data')) {
            $data = Mage::registry('installment_data')->getData();
        }
        $fieldset = $form->addFieldset('installment_form', array(
            'legend'=>Mage::helper('installment')->__('Item information')
        ));

                $fieldset->addField('product_name', 'text', array(
            'label'        => Mage::helper('installment')->__('Product Name'),
            'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'product_name',
        ));

        $fieldset->addField('sku', 'text', array(
            'label'        => Mage::helper('installment')->__('SKU'),
			'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'sku',
        ));
		
        $fieldset->addField('customer_name', 'text', array(
            'label'        => Mage::helper('installment')->__('Customer Name'),
			'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'customer_name',
        )); 
		
		$fieldset->addField('mobile', 'text', array(
            'label'        => Mage::helper('installment')->__('Mobile'),
			'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'mobile',
        ));
		
		$fieldset->addField('partner', 'text', array(
            'label'        => Mage::helper('installment')->__('Partner'),
			'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'partner',
        ));

        $fieldset->addField('installment_info', 'editor', array(
            'name'        => 'installment_info',
            'label'        => Mage::helper('installment')->__('Installment Information'),
            'title'        => Mage::helper('installment')->__('Installment Information'),
            'style'        => 'width:700px; height:500px;',
            'wysiwyg'    => false,
            'required'    => true,
        ));

		$fieldset->addField('created_at', 'date', array(
			'name'      => 'created_at',
			'label'     => Mage::helper('installment')->__('Created Date'),
			'image'     => $this->getSkinUrl('images/grid-cal.gif'),
			'format'    => 'yyyy-M-d',
			'class'     => 'required-entry',
			'required'  => true,
		));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}