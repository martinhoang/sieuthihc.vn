<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Installment Grid Block
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @author      Magestore Developer
 */
class Hchomecenter_Installment_Block_Adminhtml_Installment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('installmentGrid');
        $this->setDefaultSort('installment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * prepare collection for block to display
     *
     * @return Hchomecenter_Installment_Block_Adminhtml_Installment_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('installment/installment')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * prepare columns for this grid
     *
     * @return Hchomecenter_Installment_Block_Adminhtml_Installment_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('installment_id', array(
            'header'    => Mage::helper('installment')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'installment_id',
        ));

        $this->addColumn('product_name', array(
            'header'    => Mage::helper('installment')->__('Product Name'),
            'align'     =>'left',
            'index'     => 'product_name',
        ));
		
		$this->addColumn('sku', array(
            'header'    => Mage::helper('installment')->__('SKU'),
            'align'     =>'left',
            'index'     => 'sku',
        ));
		        
		$this->addColumn('customer_name', array(
            'header'    => Mage::helper('installment')->__('Customer Name'),
            'align'     =>'left',
            'index'     => 'customer_name',
        ));
		
        $this->addColumn('mobile', array(
            'header'    => Mage::helper('installment')->__('Mobile'),
            'align'     =>'left',
            'index'     => 'mobile',
        ));
		
		$this->addColumn('partner', array(
            'header'    => Mage::helper('installment')->__('Partner'),
            'align'     =>'left',
            'index'     => 'partner',
        ));
		
        $this->addColumn('installment_info', array(
            'header'    => Mage::helper('installment')->__('Installment Information'),
            'width'     => '150px',
            'index'     => 'installment_info',
        ));

		$this->addColumn('created_at', array(
            'header'    => Mage::helper('installment')->__('Created Date'),
            'align'     =>'left',
            'index'     => 'created_at',
			'type'      => 'date',
			'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
        )); 
		
        $this->addColumn('action',
            array(
                'header'    =>    Mage::helper('installment')->__('Action'),
                'width'        => '100',
                'type'        => 'action',
                'getter'    => 'getId',
                'actions'    => array(
                    array(
                        'caption'    => Mage::helper('installment')->__('Edit'),
                        'url'        => array('base'=> '*/*/edit'),
                        'field'        => 'id'
                    )),
                'filter'    => false,
                'sortable'    => false,
                'index'        => 'stores',
                'is_system'    => true,
        ));

        return parent::_prepareColumns();
    }
    
    /**
     * prepare mass action for this grid
     *
     * @return Hchomecenter_Installment_Block_Adminhtml_Installment_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('installment_id');
        $this->getMassactionBlock()->setFormFieldName('installment');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'        => Mage::helper('installment')->__('Delete'),
            'url'        => $this->getUrl('*/*/massDelete'),
            'confirm'    => Mage::helper('installment')->__('Are you sure?')
        ));
        return $this;
    }
    
    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}