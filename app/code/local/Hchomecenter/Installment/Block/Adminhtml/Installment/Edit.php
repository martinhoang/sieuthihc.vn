<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Installment Edit Block
 * 
 * @category     Magestore
 * @package     Magestore_Installment
 * @author      Magestore Developer
 */
class Hchomecenter_Installment_Block_Adminhtml_Installment_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'installment';
        $this->_controller = 'adminhtml_installment';
        
        $this->_updateButton('save', 'label', Mage::helper('installment')->__('Save Installment'));
        $this->_updateButton('delete', 'label', Mage::helper('installment')->__('Delete Installment'));
        
        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('installment_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'installment_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'installment_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('installment_data')
            && Mage::registry('installment_data')->getId()
        ) {
            return Mage::helper('installment')->__("Edit Installment '%s'",
                                                $this->htmlEscape(Mage::registry('installment_data')->getProductName())
            );
        }
        return Mage::helper('installment')->__('Add Installment');
    }
}