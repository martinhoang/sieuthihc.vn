<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Installment
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create installment table
 */
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('installment')};

CREATE TABLE {$this->getTable('installment')} (
  `installment_id` int(11) unsigned NOT NULL auto_increment,
  `product_name` varchar(255) NOT NULL default '',
  `sku` varchar(255) NOT NULL default '',
  `customer_name` varchar(255) NOT NULL default '',
  `mobile` varchar(255) NOT NULL default '',
  `installment_info` text NOT NULL default '',
  `created_at` datetime NULL,
  PRIMARY KEY (`installment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

