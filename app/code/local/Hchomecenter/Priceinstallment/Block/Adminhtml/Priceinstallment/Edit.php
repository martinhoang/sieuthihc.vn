<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Priceinstallment Edit Block
 * 
 * @category     Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @author      Hchomecenter Developer
 */
class Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'priceinstallment';
        $this->_controller = 'adminhtml_priceinstallment';
        
        $this->_updateButton('save', 'label', Mage::helper('priceinstallment')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('priceinstallment')->__('Delete Item'));
        
        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('priceinstallment_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'priceinstallment_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'priceinstallment_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('priceinstallment_data')
            && Mage::registry('priceinstallment_data')->getId()
        ) {
            return Mage::helper('priceinstallment')->__("Edit Item",
                                                $this->htmlEscape(Mage::registry('priceinstallment_data')->getTitle())
            );
        }
        return Mage::helper('priceinstallment')->__('Add Item');
    }
}