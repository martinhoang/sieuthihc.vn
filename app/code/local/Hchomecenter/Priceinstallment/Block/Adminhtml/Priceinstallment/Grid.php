<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Priceinstallment Grid Block
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @author      Hchomecenter Developer
 */
class Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('priceinstallmentGrid');
        $this->setDefaultSort('priceinstallment_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * prepare collection for block to display
     *
     * @return Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('priceinstallment/priceinstallment')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * prepare columns for this grid
     *
     * @return Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('priceinstallment_id', array(
            'header'    => Mage::helper('priceinstallment')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'priceinstallment_id',
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('priceinstallment')->__('SKU'),
            'align'     =>'left',
            'index'     => 'sku',
        ));
        
		$this->addColumn('price', array(
            'header'    => Mage::helper('priceinstallment')->__('Giá trả góp'),
            'align'     =>'left',
            'index'     => 'price',
        ));

		$this->addColumn('start_time', array(
            'header'    => Mage::helper('priceinstallment')->__('Thời gian bắt đầu'),
            'align'     =>'left',
            'index'     => 'start_time',
			'type'      => 'date',
			'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
        ));
		
		$this->addColumn('end_time', array(
            'header'    => Mage::helper('priceinstallment')->__('Thời gian kết thúc'),
            'align'     =>'left',
            'index'     => 'end_time',
			'type'      => 'date',
			'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
        ));
		
        $this->addColumn('action',
            array(
                'header'    =>    Mage::helper('priceinstallment')->__('Action'),
                'width'        => '100',
                'type'        => 'action',
                'getter'    => 'getId',
                'actions'    => array(
                    array(
                        'caption'    => Mage::helper('priceinstallment')->__('Edit'),
                        'url'        => array('base'=> '*/*/edit'),
                        'field'        => 'id'
                    )),
                'filter'    => false,
                'sortable'    => false,
                'index'        => 'stores',
                'is_system'    => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('priceinstallment')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('priceinstallment')->__('XML'));

        return parent::_prepareColumns();
    }
    
    /**
     * prepare mass action for this grid
     *
     * @return Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('priceinstallment_id');
        $this->getMassactionBlock()->setFormFieldName('priceinstallment');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'        => Mage::helper('priceinstallment')->__('Delete'),
            'url'        => $this->getUrl('*/*/massDelete'),
            'confirm'    => Mage::helper('priceinstallment')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('priceinstallment/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('priceinstallment')->__('Change status'),
            'url'    => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'    => 'status',
                    'type'    => 'select',
                    'class'    => 'required-entry',
                    'label'    => Mage::helper('priceinstallment')->__('Status'),
                    'values'=> $statuses
                ))
        ));
        return $this;
    }
    
    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}