<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Priceinstallment Edit Tabs Block
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @author      Hchomecenter Developer
 */
class Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('priceinstallment_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('priceinstallment')->__('Item Information'));
    }
    
    /**
     * prepare before render block to html
     *
     * @return Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('priceinstallment')->__('Item Information'),
            'title'     => Mage::helper('priceinstallment')->__('Item Information'),
            'content'   => $this->getLayout()
                                ->createBlock('priceinstallment/adminhtml_priceinstallment_edit_tab_form')
                                ->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}