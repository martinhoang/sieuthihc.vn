<?php

class Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Edit_Tab_Import extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form(array(
                                      'id' => 'config_form',
                                      'action' => $this->getUrl('*/*/importCsv'),
                                      'method' => 'post',
        							  'enctype' => 'multipart/form-data'
                                   ));
	  $form->setUseContainer(true); 
      $this->setForm($form);
      $fieldset = $form->addFieldset('import_form', array('legend'=>Mage::helper('priceinstallment')->__('Import Gifplus CSV')));	  
	  $fieldset->addField('priceinstallment_csv', 'file', array(
          'label'     => Mage::helper('priceinstallment')->__('Priceinstallment CSV File : '),
          'required'  => true,
          'name'      => 'priceinstallment_csv',
		  'after_element_html' => '</br>Lưu ý  : <small>File import cần theo đúng chuẩn mẫu.</small>',
	  ));
		
     $fieldset->addField('submit', 'submit', array(
          'value'  => 'Import',
          'after_element_html' => '<small></small>',
		  'class' => 'form-button', 			  
          'tabindex' => 1
        ));
     
      if ( Mage::getSingleton('adminhtml/session')->getImportData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getImportData());
          Mage::getSingleton('adminhtml/session')->setImportData(null);
      } elseif ( Mage::registry('import_data') ) {
          $form->setValues(Mage::registry('import_data')->getData());
      }
      return parent::_prepareForm();
  }
}