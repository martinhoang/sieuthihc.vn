<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Priceinstallment Edit Form Content Tab Block
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @author      Hchomecenter Developer
 */
class Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Hchomecenter_Priceinstallment_Block_Adminhtml_Priceinstallment_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        if (Mage::getSingleton('adminhtml/session')->getPriceinstallmentData()) {
            $data = Mage::getSingleton('adminhtml/session')->getPriceinstallmentData();
            Mage::getSingleton('adminhtml/session')->setPriceinstallmentData(null);
        } elseif (Mage::registry('priceinstallment_data')) {
            $data = Mage::registry('priceinstallment_data')->getData();
        }
        $fieldset = $form->addFieldset('priceinstallment_form', array(
            'legend'=>Mage::helper('priceinstallment')->__('Item information')
        ));

        $fieldset->addField('sku', 'text', array(
            'label'        => Mage::helper('priceinstallment')->__('SKU'),
            'class'        => 'required-entry',
            'required'    => true,
            'name'        => 'sku',
        ));

        $fieldset->addField('price', 'text', array(
            'label'        => Mage::helper('priceinstallment')->__('Giá trả góp'),
            'required'    => true,
            'name'        => 'price',
        ));
		
		$fieldset->addField('start_time', 'date', array(
			'name'      => 'start_time',
			'label'     => Mage::helper('priceinstallment')->__('Created Date'),
			'image'     => $this->getSkinUrl('images/grid-cal.gif'),
			'format'    => 'yyyy-M-d',
			'class'     => 'required-entry',
			'required'  => true,
		));
		
		$fieldset->addField('end_time', 'date', array(
			'name'      => 'end_time',
			'label'     => Mage::helper('priceinstallment')->__('Created Date'),
			'image'     => $this->getSkinUrl('images/grid-cal.gif'),
			'format'    => 'yyyy-M-d',
			'class'     => 'required-entry',
			'required'  => true,
		));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}