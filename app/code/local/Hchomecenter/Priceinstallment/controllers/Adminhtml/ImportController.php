<?php
	class Hchomecenter_Priceinstallment_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action
	{
		protected function _initAction()
		{
			$this->loadLayout()
			->_setActiveMenu('priceinstallment/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Import Priceinstallment CSV'), Mage::helper('adminhtml')->__('Import Priceinstallment CSV'));
			
			return $this;
		}   
		
		public function indexAction() 
		{
			$this->_initAction()
			->renderLayout();
		}
		
		public function importCsvAction() 
		{
			if($_FILES['priceinstallment_csv']['name'] != '') {
				$data = $this->getRequest()->getPost();
				if($data){
					try {
					//Get sku has existed in grid
						$resource = Mage::getSingleton('core/resource'); 
						$readConnection = $resource->getConnection('core_read'); 
						$writeConnection = $resource->getConnection('core_write');
						$insTable = $resource->getTableName('priceinstallment');
						$query = "select sku from $insTable";
						$results = $readConnection->fetchAll($query);
						foreach($results as $result){
							$skues[] = $result[sku];
						}
						$uploader = new Varien_File_Uploader('priceinstallment_csv');
						$uploader->setAllowedExtensions(array('csv'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$path = Mage::getBaseDir('media') . DS.'hc'.DS.'priceinstallment'.DS.'import'.DS;
						$uploader->save($path, $_FILES['priceinstallment_csv']['name'] );
						$csv = new Varien_File_Csv();
						$content = $csv->getData($path.$_FILES['priceinstallment_csv']['name']);
						for ($i=0; $i<count($content) ; $i++) {
							$sku = 'HC'.$content[$i][0];
							// Remove pre existed sku when you import
							if(in_array($sku, $skues)){
								$bindQuery = "delete from $insTable where sku = '$sku'";
								$writeConnection->query($bindQuery);
							}
							$start_day = Mage::app()->getLocale()->date(
							$content[$i][2],
							Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
							null, 
							false
							);
							$expired_day = Mage::app()->getLocale()->date(
							$content[$i][3],
							Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
							null, 
							false
							);
							$model = Mage::getModel('priceinstallment/priceinstallment')
							->setSku('HC'.$content[$i][0])
							->setPrice($content[$i][1])
							->setStartTime($start_day)
							->setEndTime($expired_day);
							$model->save();
						}
						Mage::getSingleton('core/session')->addSuccess('Import thành công.');
					}
					catch (Exception $e) {
						Mage::getModel('core/session')->addError(Mage::helper('priceinstallment')->__('Kiểu file không hợp lệ!'));
					}
					$this->_redirect('*/*/');
				}
			}
			else {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('priceinstallment')->__('Không tìm thấy file để import'));
				$this->_redirect('*/*/');
			}
		}
		protected function _isAllowed()
		{
			return true;
		}
	}																										