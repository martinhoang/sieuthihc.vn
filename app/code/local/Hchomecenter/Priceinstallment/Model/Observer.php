<?php
	/**
		* Hchomecenter
		* 
		* NOTICE OF LICENSE
		* 
		* This source file is subject to the Hchomecenter.com license that is
		* available through the world-wide-web at this URL:
		* http://www.magestore.com/license-agreement.html
		* 
		* DISCLAIMER
		* 
		* Do not edit or add to this file if you wish to upgrade this extension to newer
		* version in the future.
		* 
		* @category    Hchomecenter
		* @package     Hchomecenter_Priceinstallment
		* @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
		* @license     http://www.magestore.com/license-agreement.html
	*/
	
	/**
		* Priceinstallment Observer Model
		* 
		* @category    Hchomecenter
		* @package     Hchomecenter_Priceinstallment
		* @author      Hchomecenter Developer
	*/
	class Hchomecenter_Priceinstallment_Model_Observer
	{
		/**
			* process controller_action_predispatch event
			*
			* @return Hchomecenter_Priceinstallment_Model_Observer
		*/
		public function deleteOverdueProductZeroPercent()
		{
			$today = Mage::getModel('core/date')->date('Y-m-d H:i:s');
			$resource = Mage::getSingleton('core/resource');
			$write = $resource->getConnection('core_write');
			$insTable = $resource->getTableName('priceinstallment');
			$bindQuery = "DELETE FROM $insTable WHERE `end_time` < '$today' ";
			$write->query($bindQuery);
		}
	}	