<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Priceinstallment Resource Collection Model
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_Priceinstallment
 * @author      Hchomecenter Developer
 */
class Hchomecenter_Priceinstallment_Model_Mysql4_Priceinstallment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('priceinstallment/priceinstallment');
    }
}