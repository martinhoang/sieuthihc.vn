<?php
/**
 * Hchomecenter
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Hchomecenter.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Hchomecenter
 * @package     Hchomecenter_PriceInstallment
 * @copyright   Copyright (c) 2012 Hchomecenter (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create priceinstallment table
 */
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('priceinstallment')};

CREATE TABLE {$this->getTable('priceinstallment')} (
  `priceinstallment_id` int(11) unsigned NOT NULL auto_increment,
  `sku` varchar(255) NOT NULL default '',
  `price` varchar(255) NOT NULL default '',
  `start_time` datetime NULL,
  `end_time` datetime NULL,
  PRIMARY KEY (`priceinstallment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

