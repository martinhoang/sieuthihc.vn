<?php
	
	class Hchomecenter_PriceChange_Model_Observer  {
		
		public function changePrice(Varient_Event_Observer $observer) {
			$params = Mage::app()->getRequest()->getPost();
			$id = $params['proId'];
			$product = Mage::getModel('catalog/product')->load($id);
			$price_show = $product->getResource()->getAttribute('price_show');
			$price_show_value = $price_show->getFrontend()->getValue($product);
			$price = $product->getPrice();
			$specialPrice = $product->getSpecialPrice();
			$from = $product->getResource()->getAttribute('show_from_date');
			$from_value = $from->getFrontend()->getValue($product);
			$to = $product->getResource()->getAttribute('show_to_date');
			$to_value = $to->getFrontend()->getValue($product);
			$special_from_date = $product->getSpecialFromDate();
			$special_to_date = $product->getSpecialToDate();
			$today = Mage::getModel('core/date')->date('Y-m-d');
			if(!empty($specialPrice) && strtotime($today)<=strtotime($special_to_date) && strtotime($today)>=strtotime($special_from_date)){
				$new_price = $specialPrice;
			}
			else{
				if(!empty($price_show_value) && strtotime($today)<=strtotime($to_value) && strtotime($today)>=strtotime($from_value)){
					$new_price = $price_show_value;
				}
				else{
					$new_price = $price;
				}
			}
			$item = $observer->getQuoteItem();
			$item = ( $item->getParentItem() ? $item->getParentItem() : $item );
			$item->setCustomPrice($new_price);
			$item->setOriginalCustomPrice($new_price);
			$item->getProduct()->setIsSuperMode(true);
		}
	}
