<?php
class Hchomecenter_Saleoff_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        /* $cache = Mage::app()->getCache();
        $cachedKey = 'cache/saleoff';
        $resetPage = $this->getRequest()->getParam('reset_cache');
        if ($resetPage == 1) {
            $cache->remove($cachedKey);
        }
        $output = $cache->load($cachedKey);
        if (!empty($output)) {
            echo $output;
            exit();
        } */
        $this->loadLayout();
        $layout = $this->getLayout();
        if ($layout != NULL) {
            $head = $layout->getBlock('head');
            $defaultTitle = 'Khuyến mãi điện máy';
            $defaultKeywords = 'khuyen mai, khuyen mai dien may, sieu thi dien may, dien may hc';
            // Set Data
            $head->setTitle($defaultTitle);
            $head->setDescription($defaultTitle);
            $head->setKeywords($defaultKeywords);
        }
		$output = $this->getLayout()->getOutput();
        echo $output;
       /*  $cache->save($output, $cachedKey, array('cache/saleoff'), 999999); */
    }

    public function viewAction()
    {
        $cache = Mage::app()->getCache();
        $saleoffId = $this->getRequest()->getParam('id');
        if ($saleoffId == 117) {
            header('Location: http://hc.com.vn/khuyen-mai/thang-vang-ron-rang-khai-truong');
            exit();
        }
        $cachedKey = 'cache/saleoff/' . $saleoffId;
        $resetPage = $this->getRequest()->getParam('reset_cache');
        if ($resetPage == 1) {
            $cache->remove($cachedKey);
        }
        $output = $cache->load($cachedKey);
        if (!empty($output)) {
            echo $output;
            exit();
        }
        if ($saleoffId != null && $saleoffId != '') {
            $saleoff = Mage::getModel('saleoff/saleoff')->load($saleoffId);
            $link = $saleoff->getLink();
            if (!empty($link)) {
                header('Location: ' . $link);
                exit();
            }
        } else {
            $saleoff = null;
            exit();
        }
        // restricted
        if ($saleoff->getId() && $saleoff->getStatus() != 1) {
            $this->_redirect('saleoff');
            exit();
        }

        Mage::register('saleoff', $saleoff);
        $this->loadLayout();
        $layout = $this->getLayout();
        if ($layout != NULL) {
            $head = $layout->getBlock('head');
            $content = $layout->getBlock('content');
            $content->setDataEntity($saleoff);
            $defaultTitle = 'Khuyến mãi điện máy';
            $defaultDesc = $saleoff->getShortDescription();
            if (!$defaultDesc) {
                $defaultDesc = $saleoff->getTitle();
            }
            $defaultKeywords = 'khuyen mai, khuyen mai dien may, sieu thi dien may, dien may hc';

            // Get Meta Data
            $title = (!$saleoff->getTitle()) ?  $defaultTitle : $saleoff->getTitle();
            $desc = (!$saleoff->getDescription()) ?  $defaultDesc : $saleoff->getDescription();
            $keywords = (!$saleoff->getKeywords()) ?  $defaultKeywords : $saleoff->getKeywords();

            // Set Data
            $head->setTitle($title);
            $head->setDescription($desc);
            $head->setKeywords($keywords);
        }

        $output = $this->getLayout()->getOutput();
        echo $output;

        $cache->save($output, $cachedKey, array('cache/saleoff'), 999999);
    }
}