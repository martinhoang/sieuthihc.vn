<?php

class Hchomecenter_Saleoff_AjaxController extends Mage_Adminhtml_Controller_Action {

    public function bindAction() {
        try {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('catalog_write');

            $saleoffProductTable = $resource->getTableName('saleoff_products');

            $saleoffId = $this->getRequest()->getParam('saleoffId');
            $productId = $this->getRequest()->getParam('productId');
            if ($saleoffId != 0) {
                $bindQuery = "insert into $saleoffProductTable(saleoff_id, product_id) values ($saleoffId, $productId)";
                $write->query($bindQuery);
            } else {
                // Add to session data
                if (!Mage::getSingleton('core/session')->getGiftProductIds()) {
                    $productIds = array();
                } else {
                    $productIds = Mage::getSingleton('core/session')->getGiftProductIds();
                }
                $productIds[$productId] = $productId;
                Mage::getSingleton('core/session')->setGiftProductIds($productIds);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function unbindAction() {
        try {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('catalog_write');

            $saleoffProductTable = $resource->getTableName('saleoff_products');

            $saleoffId = $this->getRequest()->getParam('saleoffId');
            $productId = $this->getRequest()->getParam('productId');
            if ($saleoffId != 0) {
                $unbindQuery = "delete from $saleoffProductTable where product_id = $productId and saleoff_id = $saleoffId";
                $write->query($unbindQuery);
            } else {
                // Add to session data
                if (!Mage::getSingleton('core/session')->getGiftProductIds()) {
                    $productIds = array();
                } else {
                    $productIds = Mage::getSingleton('core/session')->getGiftProductIds();
                }
                unset($productIds[$productId]);
                Mage::getSingleton('core/session')->setGiftProductIds($productIds);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }
	protected function _isAllowed()
	{
		return true;
	}
}