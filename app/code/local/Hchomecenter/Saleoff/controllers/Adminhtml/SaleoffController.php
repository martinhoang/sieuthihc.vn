<?php

class Hchomecenter_Saleoff_Adminhtml_SaleoffController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('saleoff/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}

    public function gridAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('saleoff.products')
            ->setSelectedProducts($this->getRequest()->getPost('selected_products', null));
        $this->renderLayout();
    }

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('saleoff/saleoff')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('saleoff_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('saleoff/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
                $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            }

			$this->_addContent($this->getLayout()->createBlock('saleoff/adminhtml_saleoff_edit'))
				->_addLeft($this->getLayout()->createBlock('saleoff/adminhtml_saleoff_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('saleoff')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
        Mage::getSingleton('core/session')->setSaleoffProductIds(null);
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {

			if (!empty($data['identifier'])) {
                $data['identifier'] = Mage::helper('saleoff')->renderToAlias($data['identifier']);
            } else {
                $data['identifier'] = Mage::helper('saleoff')->renderToAlias($data['title']);
            }

			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS . 'saleoff/khuyen-mai'. DS .'avatar';
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			} else {
                unset($data['filename']);
            }

            if(isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('thumbnail');

                    // Any extention would work
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(false);

                    // Set the file upload mode
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);

                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS ;
                    $uploader->save($path, $_FILES['thumbnail']['name'] );

                } catch (Exception $e) {

                }

                //this way the name is saved in DB
                $data['thumbnail'] = $_FILES['thumbnail']['name'];
            } else {
                unset($data['thumbnail']);
            }

            if(isset($_FILES['zip']['name']) && $_FILES['zip']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('zip');

                    // Any extention would work
                    $uploader->setAllowedExtensions(array('zip'));
                    $uploader->setAllowRenameFiles(false);

                    // Set the file upload mode
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);

                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS . 'saleoff' . DS . $data['identifier'] . DS;
                    $files = glob(Mage::getBaseDir('media') . '/saleoff/'. $data['identifier'] . '/*'); // get all file names
                    foreach($files as $file){
                        if(is_file($file)) {
                            @unlink($file);
                        }
                    }
                    @rmdir($path);
                    $uploader->save($path, $_FILES['zip']['name']);
                    $fileName = $path . $_FILES['zip']['name'];

                    // Explode File Zip
                    $zip = new ZipArchive;
                    $res = $zip->open($fileName);
                    if ($res === TRUE) {
                        $zip->extractTo(str_replace($_FILES['zip']['name'], '', Mage::getBaseDir('media') . DS . 'saleoff' . DS . $data['identifier'] . DS));
                        $zip->close();
                    } else {
                        die('Error When Extract File Zip:' . $res);
                    }
                    @unlink($fileName);

                } catch (Exception $e) {

                }
            }
	  		unset($data['zip']);
	  		/* $data['text_content']=trim(preg_replace('/\s+/', ' ', $data['text_content'])); */
			$data['text_content']= preg_replace('/^\s+|\n|\r|\s+$/m', '', $data['text_content']);
			$model = Mage::getModel('saleoff/saleoff');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();

                $resource = Mage::getSingleton('core/resource');
                $write = $resource->getConnection('catalog_write');
                $saleoffProductTable = $resource->getTableName('saleoffplus_products');
                $productIds = Mage::getSingleton('core/session')->getSaleoffProductIds();
                if (count($productIds) > 0) {
                    $id = $this->getRequest()->getParam('id');
                    if (!$id) {
                        $id = $model->getId();
                        $bindQuery = "delete from $saleoffProductTable where saleoff_id = " . $id;
                        $write->query($bindQuery);
                        foreach ($productIds as $productId) {
                            $bindQuery = "insert into $saleoffProductTable(saleoff_id, product_id) values ($id, $productId)";
                            $write->query($bindQuery);
                        }
                    }
                }

                $cachedKey = 'cache/saleoff/' . $model->getId();
                $cachedListKey = 'cache/saleoff';
                $cache = Mage::app()->getCache();
                $cache->remove($cachedKey);
                $cache->remove($cachedListKey);

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('saleoff')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('saleoff')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('saleoff/saleoff');
				// Delete Images;
                $baseImage = Mage::getBaseDir('media') . DS;
                $filename = $baseImage . $model->getImage();
                @unlink($filename);
                $thumbnail = $baseImage . $model->getThumbnail();
                @unlink($thumbnail);
                $files = glob(Mage::getBaseDir('media') . '/saleoff/'. $model->getIndentifier() . '/*');
                foreach($files as $file){
                    if(is_file($file)) {
                        @unlink($file);
                    }
                }
                @rmdir(Mage::getBaseDir('media') . '/saleoff/'. $model->getIndentifier());

				$model->setId($this->getRequest()->getParam('id'))
					->delete();

                $cachedKey = 'cache/saleoff/' . $this->getRequest()->getParam('id');
                $cachedListKey = 'cache/saleoff';
                $cache = Mage::app()->getCache();
                $cache->remove($cachedKey);
                $cache->remove($cachedListKey);

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $saleoffIds = $this->getRequest()->getParam('saleoff');
        if(!is_array($saleoffIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                $cachedListKey = 'cache/saleoff';
                $cache = Mage::app()->getCache();
                foreach ($saleoffIds as $saleoffId) {
                    $saleoff = Mage::getModel('saleoff/saleoff')->load($saleoffId);
                    $saleoff->delete();
                    $cachedKey = 'cache/saleoff/' . $saleoffId;
                    $cache->remove($cachedKey);
                }
                $cache->remove($cachedListKey);
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($saleoffIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $saleoffIds = $this->getRequest()->getParam('saleoff');
        if(!is_array($saleoffIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $cachedListKey = 'cache/saleoff';
                $cache = Mage::app()->getCache();
                foreach ($saleoffIds as $saleoffId) {
                    $saleoff = Mage::getSingleton('saleoff/saleoff')
                        ->load($saleoffId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                    $cachedKey = 'cache/saleoff/' . $saleoffId;
                    $cache->remove($cachedKey);
                }
                $cache->remove($cachedListKey);
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($saleoffIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'saleoff.csv';
        $content    = $this->getLayout()->createBlock('saleoff/adminhtml_saleoff_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'saleoff.xml';
        $content    = $this->getLayout()->createBlock('saleoff/adminhtml_saleoff_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
	}
	protected function _isAllowed()
	{
		return true;
	}
}