<?php
/*------------------------------------------------------------------------
 # SM Market - Version 1.0
 # Copyright (c) 2014 The YouTech Company. All Rights Reserved.
 # @license - Copyrighted Commercial Software
 # Author: YouTech Company
 # Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

class Hchomecenter_Saleoff_Model_System_Config_Source_Proshow
{
	public function toOptionArray()
	{	
		$option=array();
		$resource    = Mage::getSingleton('core/resource'); 
        $read        = $resource->getConnection('catalog_read'); 
        $saleTable = 'saleoff';
        $query = 'select * from saleoff where status=1 and now()BETWEEN start_day and expired_day order by saleoff_id DESC';
        $select      = $read->select($query);                            
        $res         = $read->fetchAll($query);
		/* $collection = Mage::getModel('saleoff/saleoff')->getCollection()
					->addFieldToFilter('status',1); */
		foreach($res as $col){
			$option[]=array('value'=>$col['saleoff_id'],'label'=>$col['title']);
		}
		return $option;
	}
}
