<?php

class Hchomecenter_Saleoff_Model_Saleoff extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('saleoff/saleoff');
    }

    public function getProducts() {
        if (!$this->getId()) {
            return array();
        }
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('catalog_read');
        $giftProductTable = $resource->getTableName('saleoff_products');
        $selectQuery = "select product_id from $giftProductTable where saleoff_id = " . $this->getId();
        $res = $read->fetchAll($selectQuery);
        $collection = array();
        if (count($res) > 0) {
            foreach ($res as $row) {
                $collection[$row['product_id']] = $row['product_id'];
            }
        }
        return $collection;
    }

    public function getProductSelected() {
        $productIds = array();
        if (Mage::getSingleton('core/session')->getSaleoffProductIds()) {
            $productIds = Mage::getSingleton('core/session')->getSaleoffProductIds();
        }
        return $productIds;
    }
}