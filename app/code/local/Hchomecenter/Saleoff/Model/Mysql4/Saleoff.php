<?php

class Hchomecenter_Saleoff_Model_Mysql4_Saleoff extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the saleoff_id refers to the key field in your database table.
        $this->_init('saleoff/saleoff', 'saleoff_id');
    }
}