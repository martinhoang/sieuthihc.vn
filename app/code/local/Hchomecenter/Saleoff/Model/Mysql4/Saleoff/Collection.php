<?php

class Hchomecenter_Saleoff_Model_Mysql4_Saleoff_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('saleoff/saleoff');
    }
}