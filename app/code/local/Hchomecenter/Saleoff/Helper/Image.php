<?php

class Hchomecenter_Saleoff_Helper_Image extends Mage_Core_Helper_Abstract
{
	/* public function resizeImg($fileName,$width,$height = ''){
    $folderURL = Mage::getBaseDir('media') . DS . 'saleoff/khuyen-mai'. DS .'avatar';
    $imageURL = $folderURL .DS. $fileName;
 
    $basePath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'saleoff/khuyen-mai'. DS .'avatar' . DS . $fileName;
    $newPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'saleoff/khuyen-mai'. DS .'avatar' . DS .'resize';
   
    if ($width != '') {
        //if image has already resized then just return URL
        if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
           $imageObj->constrainOnly ( true );
			$imageObj->keepAspectRatio ( true );
			$imageObj->keepFrame ( true ); // force Frame
			
			$imageObj->keepTransparency(true);  // keep Transparency with image png
			$imageObj->backgroundColor(array(255,255,255));
			$imageObj->resize ($width, $height);
            $imageObj->save($newPath);
        }
        $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized" . DS . $fileName;
     } else {
        $resizedURL = $imageURL;
     }
     return $resizedURL;
} */
    public function getResizedUrl($imgUrl,$x=100,$y=100,$color){


		$imgPath=$this->splitImageValue($imgUrl,"path");
		$imgName=$this->splitImageValue($imgUrl,"name");
		$imgPath=str_replace("/",DS,$imgPath);
		$imgPathFull=Mage::getBaseDir("media").DS.$imgPath.DS.$imgName;
		$width=$x;
		$y?$height=$y:$height=$x;
		$resizeFolder=$width."X".$height;		 
		$imageResizedPath = Mage::getBaseDir("media").DS.$imgPath.DS.$resizeFolder.DS.$imgName;
		$colorArray = array();
		$colorArray = explode(",",$color);
		if (!file_exists($imageResizedPath) && file_exists($imgPathFull)) :
			
			$imageObj = new Varien_Image($imgPathFull);
			$imageObj->constrainOnly ( true );
			$imageObj->keepAspectRatio ( true );
			$imageObj->keepFrame ( true ); // force Frame
			$imageObj->keepTransparency(true);  // keep Transparency with image png
			$imageObj->backgroundColor(array(255,255,255));
			$imageObj->resize ($width, $height);
			$imageObj->save($imageResizedPath);   
		endif;

		$imgUrl=str_replace(DS,"/",$imgPath);

		return Mage::getBaseUrl("media").$imgUrl."/".$resizeFolder."/".$imgName;
	}
	public function splitImageValue($imageValue,$attr="name"){
		$imArray=explode("/",$imageValue);

		$name=$imArray[count($imArray)-1];
		$path=implode("/",array_diff($imArray,array($name)));
		if($attr=="path"){
			return $path;
		}
		else
			return $name;

	}

}