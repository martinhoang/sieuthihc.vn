<?php

class Hchomecenter_Saleoff_Helper_Data extends Mage_Core_Helper_Abstract
{

	const ROUTER_PROMOTION='khuyen-mai';
    public function renderToAlias($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(,|_)/", ' ', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = str_replace(", ", " ", $str);
        $str = str_replace("--", " ", $str);
        $str = trim($str);
        $str = strtolower(str_replace(" ", "-", $str));
        return $str;
    }

    function formatDate($dateStr, $format = 'd/m/Y') {
        $date = new DateTime($dateStr);
        return $date->format($format);
    }

    function getImageSrc($filename) {
        return $this->getBaseUrl() . 'media/' . $filename;
    }

    function getSaleoffUrl($identifier) {
        return Mage::getBaseUrl() . 'khuyen-mai/' . $identifier . '.html';
    }

    function renderImageSrc($identifier, $filename) {
        if (file_exists(Mage::getBaseDir() . '/media/saleoff/' . $identifier . '/images/')) {
            return $this->getBaseUrl() . 'media/saleoff/' . $identifier . '/images/' . $filename;
        }
        return $this->getBaseUrl() . 'media/saleoff/' . $identifier . '/' . $filename;
    }

    function getBaseUrl() {
        return str_replace('index.php/', '', Mage::getBaseUrl());
    }
}