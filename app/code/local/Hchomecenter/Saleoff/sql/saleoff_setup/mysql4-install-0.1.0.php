<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('saleoff')};
CREATE TABLE {$this->getTable('saleoff')} (
  `saleoff_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `identifier` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `icon` varchar(255) NOT NULL default '',
  `description` varchar(525) NOT NULL default '',
  `keywords` varchar(525) NOT NULL default '',
  `content` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `start_day` datetime NULL,
  `expired_day` datetime NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`saleoff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE {$this->getTable('saleoff_products')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `saleoff_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 