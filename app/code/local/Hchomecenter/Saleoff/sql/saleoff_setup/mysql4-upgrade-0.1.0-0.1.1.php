<?php
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('saleoff')} ADD  `text_content` text NOT NULL default '';
");
$installer->endSetup();