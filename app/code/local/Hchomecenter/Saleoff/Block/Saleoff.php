<?php
class Hchomecenter_Saleoff_Block_Saleoff extends Mage_Catalog_Block_Product_List
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getSaleoff()     
     { 
        if (!$this->hasData('saleoff')) {
            $this->setData('saleoff', Mage::registry('saleoff'));
        }
        return $this->getData('saleoff');
    }

    public function getCollection() {
        $collection = Mage::getModel('saleoff/saleoff')
                          ->setAttributeToSelect('*')
                          ->setAttributeToFilter('status', 1)
                          ->setAttributeToOrder('start_day', 'DESC')
                          ->getCollection();
        return $collection;
    }
    
    public function getPromo()
    {
        $resource    = Mage::getSingleton('core/resource'); 
        $read        = $resource->getConnection('catalog_read'); 
        $saleTable = 'saleoff';
        $query = 'select * from saleoff where status=1 and now()BETWEEN start_day and expired_day order by saleoff_id DESC';
        $select      = $read->select($query);                            
        $res         = $read->fetchAll($query); 
        return $res;
    }

    public function getImageList($saleoff = null) {
        if ($saleoff == null) {
            return array();
        }
        if (file_exists(Mage::getBaseDir() . '/media/saleoff/' . $saleoff->getIdentifier() . '/images/')) {
            $dir = Mage::getBaseDir() . DS . 'media' . DS . 'saleoff' . DS . $saleoff->getIdentifier() . DS . 'images' . DS;
        } else {
            $dir = Mage::getBaseDir() . DS . 'media' . DS . 'saleoff' . DS . $saleoff->getIdentifier() . DS;
        }
        $files = scandir($dir);
        return $files;
    }
}