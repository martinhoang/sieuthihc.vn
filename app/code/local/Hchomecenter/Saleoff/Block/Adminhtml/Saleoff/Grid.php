<?php

class Hchomecenter_Saleoff_Block_Adminhtml_Saleoff_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('saleoffGrid');
      $this->setDefaultSort('start_day');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('saleoff/saleoff')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('saleoff_id', array(
          'header'    => Mage::helper('saleoff')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'saleoff_id',
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('saleoff')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));

      $this->addColumn('start_day', array(
          'header'    => Mage::helper('saleoff')->__('Start day'),
          'align'     =>'left',
          'index'     => 'start_day',
      ));

      $this->addColumn('expired_day', array(
          'header'    => Mage::helper('saleoff')->__('Expired day'),
          'align'     =>'left',
          'index'     => 'expired_day',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('saleoff')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('saleoff')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('saleoff')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('saleoff')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('saleoff')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('saleoff')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('saleoff_id');
        $this->getMassactionBlock()->setFormFieldName('saleoff');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('saleoff')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('saleoff')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('saleoff/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('saleoff')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('saleoff')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}