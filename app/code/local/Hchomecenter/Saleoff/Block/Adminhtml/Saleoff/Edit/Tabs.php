<?php

class Hchomecenter_Saleoff_Block_Adminhtml_Saleoff_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('saleoff_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('saleoff')->__('Item #'.$this->getRequest()->getParam('id').' Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('saleoff')->__('Item Information'),
          'title'     => Mage::helper('saleoff')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('saleoff/adminhtml_saleoff_edit_tab_form')->toHtml(),
      ));

      $this->addTab('product_section', array(
          'label' => Mage::helper('saleoff')->__('Apply To Products'),
          'title' => Mage::helper('saleoff')->__('Apply To Products'),
          'content' => $this->getLayout()->createBlock('saleoff/adminhtml_saleoff_edit_tab_product')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}