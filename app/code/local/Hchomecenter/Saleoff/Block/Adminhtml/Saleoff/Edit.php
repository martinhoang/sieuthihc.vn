<?php

class Hchomecenter_Saleoff_Block_Adminhtml_Saleoff_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'saleoff';
        $this->_controller = 'adminhtml_saleoff';
        
        $this->_updateButton('save', 'label', Mage::helper('saleoff')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('saleoff')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('saleoff_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'saleoff_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'saleoff_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('saleoff_data') && Mage::registry('saleoff_data')->getId() ) {
            return Mage::helper('saleoff')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('saleoff_data')->getTitle()));
        } else {
            return Mage::helper('saleoff')->__('Add Item');
        }
    }
}