<?php

class Hchomecenter_Saleoff_Block_Adminhtml_Saleoff_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('saleoff_form', array('legend' => Mage::helper('saleoff')->__('Item information')));

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('saleoff')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('identifier', 'text', array(
            'label' => Mage::helper('saleoff')->__('Identifier'),
            'required' => false,
            'name' => 'identifier',
        ));

        $fieldset->addField('link', 'text', array(
            'label' => Mage::helper('saleoff')->__('Link'),
            'required' => false,
            'name' => 'link',
        ));
		try {
			$object = Mage::getModel('saleoff/saleoff')->load($this->getRequest()->getParam('id'));
			$note = false;
			$img = '';		
			if($object->getFilename()) {
				if($object->getFilename() != "" && $object->getFilename() != NULL) {
					$imageFile = 'saleoff/khuyen-mai/avatar/'.$object->getFilename();				
					If ($imageFile) {
						$img = '<img src="' . Mage::helper('saleoff/image')->getResizedUrl($imageFile,100,48,'') . '" border="0" align="center"/>';
					} 
				} 
			} else {
					$str = "saleoff/khuyen-mai/avatar/no_image_available.png";			
					$imge = ltrim(rtrim($str));
					$img = '<img src="' . Mage::helper('saleoff/image')->getResizedUrl($str,100,48,'') . '" border="0" align="center"/>';
			}
			
		} catch (Exception $e) {
			$str = "saleoff/khuyen-mai/avatar/no_image_available.png";			
			$imge = ltrim(rtrim($str));
			$img = '<img src="' . Mage::helper('saleoff/image')->getResizedUrl($str,100,48,'') . '" border="0" align="center"/>';
		}
        $fieldset->addField('filename', 'file', array(
            'label' => Mage::helper('saleoff')->__('File'),
            'required' => false,
            'name' => 'filename',
            /* 'image' => $this->getSkinUrl('images/grid-cal.gif') */
			'after_element_html' => $img,
        ));

        $fieldset->addField('zip', 'file', array(
            'label' => Mage::helper('saleoff')->__('ZIP Images'),
            'required' => false,
            'name' => 'zip',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('saleoff')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('saleoff')->__('Enabled'),
                ),

                array(
                    'value' => 2,
                    'label' => Mage::helper('saleoff')->__('Disabled'),
                ),
            ),
        ));

        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => Mage::helper('saleoff')->__('Description'),
            'title' => Mage::helper('saleoff')->__('Description'),
            'style' => 'width:700px; height:100px;',
            'wysiwyg' => false,
            'required' => false,
        ));

        $fieldset->addField('text_content', 'editor', array(
            'name' => 'text_content',
            'label' => Mage::helper('saleoff')->__('Gắn liên kết'),
            'title' => Mage::helper('saleoff')->__('Gắn liên kết'),
            'style' => 'width:700px; height:100px;',
            'wysiwyg' => false,
            'required' => false,
        ));

        $fieldset->addField('seo_content', 'editor', array(
            'name' => 'seo_content',
            'label' => Mage::helper('saleoff')->__('Nội dung tối ưu SEO'),
            'title' => Mage::helper('saleoff')->__('Nội dung tối ưu SEO'),
            'style' => 'width:700px; height:100px;',
            'wysiwyg' => true,
            'required' => false,
        ));

        $fieldset->addField('keywords', 'editor', array(
            'name' => 'keywords',
            'label' => Mage::helper('saleoff')->__('Keywords'),
            'title' => Mage::helper('saleoff')->__('Keywords'),
            'style' => 'width:700px; height:100px;',
            'wysiwyg' => false,
            'required' => false,
        ));

        $fieldset->addField('start_day', 'date', array(
            'name'      => 'start_day',
            'label'     => Mage::helper('saleoff')->__('Start Day'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => 'yyyy-M-d',
            'class'     => 'required-entry',
            'required'  => true,
        ));

        $fieldset->addField('expired_day', 'date', array(
            'name'      => 'expired_day',
            'label'     => Mage::helper('saleoff')->__('Expired Day'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => 'yyyy-M-d',
            'required'  => true,
        ));

        /*
        $fieldset->addField('links', 'editor', array(
            'name' => 'links',
            'label' => Mage::helper('saleoff')->__('Gắn liên kết'),
            'title' => Mage::helper('saleoff')->__('Gắn liên kết'),
            'style' => 'width:700px; height:500px;',
            'wysiwyg' => false,
            'required' => false,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig()
        ));
        */

        if (Mage::getSingleton('adminhtml/session')->getSaleoffData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getSaleoffData());
            Mage::getSingleton('adminhtml/session')->setSaleoffData(null);
        } elseif (Mage::registry('saleoff_data')) {
            $form->setValues(Mage::registry('saleoff_data')->getData());
        }
        return parent::_prepareForm();
    }
}