<?php
class Hchomecenter_Saleoff_Block_Adminhtml_Saleoff extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_saleoff';
    $this->_blockGroup = 'saleoff';
    $this->_headerText = Mage::helper('saleoff')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('saleoff')->__('Add Item');
    parent::__construct();
  }
}