<?php

/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 * 
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 */
class Hchomecenter_Saleoff_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract {

    public function initControllerRouters($observer) {
        $front = $observer->getEvent()->getFront();
        $saleoff = new Hchomecenter_Saleoff_Controller_Router();
        $front->addRouter('saleoff', $saleoff);
    }

    public function match(Zend_Controller_Request_Http $request) {
        $identifier = $request->getPathInfo();
        if (strstr($identifier, 'khuyen-mai/vip') || strstr($identifier, 'khuyen-mai/voucher-vip')) {
            header('Location: http://hc.com.vn/vip');
            //header('Location: http://hc.com.vn');
            exit();
        }
        if (strstr($identifier, 'offer-3tr')) {
            $identifier = '/khuyen-mai/mua-online-giam-den-3-trieu-dong';
        }
        if (strstr($identifier, 'vip')) {
            $identifier = '/khuyen-mai/vip';
        }
        if (strstr($identifier, 'ecomviet')) {
            $identifier = '/khuyen-mai/ecomviet';
        }
        if (!strstr($identifier, 'khuyen-mai')) {
            return false;
        }
        if (!Mage::app()->isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
        }
        $this->getFront()->setDefault(array(
            'module' =>     'saleoff',
            'controller' => 'index',
            'action' =>     'index'
        ));
        $identifier = str_replace(array('.html', '/khuyen-mai/', 'saleoff'), '', $identifier);
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('catalog_read');
        $saleoffTable = $resource->getTableName('saleoff');
        $selectQuery = "select saleoff_id from $saleoffTable where identifier = '" . $identifier . "' limit 0, 1";
        $res = $read->fetchRow($selectQuery);
        if (!empty($res['saleoff_id'])) {
            $request->setModuleName('saleoff');
            $request->setControllerName('index');
            $request->setActionName('view');
            $request->setParam('id', $res['saleoff_id']);
            $request->setParam('identifier', $identifier);
        } else {
            $request->setModuleName('saleoff');
            $request->setControllerName('index');
            $request->setActionName('index');
        }
        return true;
    }

}
