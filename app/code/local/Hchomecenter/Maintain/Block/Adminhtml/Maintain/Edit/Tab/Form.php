<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Maintain Edit Form Content Tab Block
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @author      Magestore Developer
 */
class Hchomecenter_Maintain_Block_Adminhtml_Maintain_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Hchomecenter_Maintain_Block_Adminhtml_Maintain_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
		$admin_user_session = Mage::getSingleton('admin/session');
		$adminuserId = $admin_user_session->getUser()->getUserId();
		$role_data = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getData();
		$role_name = $role_data['role_name'];
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        if (Mage::getSingleton('adminhtml/session')->getMaintainData()) {
            $data = Mage::getSingleton('adminhtml/session')->getMaintainData();
            Mage::getSingleton('adminhtml/session')->setMaintainData(null);
        } elseif (Mage::registry('maintain_data')) {
            $data = Mage::registry('maintain_data')->getData();
        }
        $fieldset = $form->addFieldset('maintain_form', array(
            'legend'=>Mage::helper('maintain')->__('Item information')
        ));
		if($role_name =="Administrators" || $role_name =="SuperMaintainer"){
			$fieldset->addField('manufacture', 'text', array(
				'label'        => Mage::helper('maintain')->__('Hãng'),
				'class'        => 'required-entry',
				'required'    => true,
				'name'        => 'manufacture',
			));
			
			$fieldset->addField('maintain_info', 'editor', array(
				'name'      => 'maintain_info',
				'label'     => Mage::helper('maintain')->__('Thông tin'),
				'title'     => Mage::helper('maintain')->__('Thông tin'),
				'style'     => 'width:700px; height:500px;',
				'wysiwyg'   => false,
				'required'  => true,
			));
			
			$fieldset->addField('customer_name', 'text', array(
				'label'        => Mage::helper('maintain')->__('Họ tên'),
				'class'        => 'required-entry',
				'required'    => true,
				'name'        => 'customer_name',
			)); 
			
			$fieldset->addField('mobile', 'text', array(
				'label'        => Mage::helper('maintain')->__('Mobile'),
				'class'        => 'required-entry',
				'required'    => true,
				'name'        => 'mobile',
			));
			
			$fieldset->addField('email', 'text', array(
				'label'        => Mage::helper('maintain')->__('Email'),
				'class'        => 'required-entry',
				'required'    => false,
				'name'        => 'email',
			));
			
			$fieldset->addField('address', 'text', array(
				'label'        => Mage::helper('maintain')->__('Địa chỉ'),
				'class'        => 'required-entry',
				'required'    => false,
				'name'        => 'address',
			));
		}
		if($role_name =="Administrators" || $role_name =="CSKH" || $role_name =="SuperMaintainer"){
			$fieldset->addField('supporter_call', 'select', array(
            'label'        => Mage::helper('maintain')->__('Tình trạng CSKH gọi'),
            'name'        => 'supporter_call',
			'values'    => array(
					array(
					  'value'     => 'Đã gọi',
					  'label'     => Mage::helper('maintain')->__('Đã gọi'),
					),
			
					array(
					  'value'     => 'Chưa gọi',
					  'label'     => Mage::helper('maintain')->__('Chưa gọi'),
					),
					
					array(
					  'value'     => 'Không liên lạc được',
					  'label'     => Mage::helper('maintain')->__('Không liên lạc được'),
					),
				),
			));
					
			$fieldset->addField('appointment_time', 'date', array(
				'name'      => 'created_at',
				'label'     => Mage::helper('maintain')->__('Ngày đã hẹn'),
				'image'     => $this->getSkinUrl('images/grid-cal.gif'),
				'format'    => 'yyyy-M-d',
				'required'  => false,
			));
					
			$fieldset->addField('source_register', 'select', array(
				'label'        => Mage::helper('maintain')->__('Nguồn đăng ký'),
				'name'        => 'source_register',
				'values'    => array(
					array(
					  'value'     => 'Đăng ký Online qua Website',
					  'label'     => Mage::helper('maintain')->__('Đăng ký Online qua Website'),
					),
					
					array(
					  'value'     => 'Đăng ký bằng email gửi tới online@hc.com.vn',
					  'label'     => Mage::helper('maintain')->__('Đăng ký bằng email gửi tới online@hc.com.vn'),
					),
			
					array(
					  'value'     => 'Đăng ký trực tiếp tại các chi nhánh HC gần nhất',
					  'label'     => Mage::helper('maintain')->__('Đăng ký trực tiếp tại các chi nhánh HC gần nhất'),
					),
					
					array(
					  'value'     => 'Đăng ký qua hotline 19001788',
					  'label'     => Mage::helper('maintain')->__('Đăng ký qua hotline 19001788'),
					),
				),
			));
		}		
		
		if($role_name =="Administrators" || $role_name =="Maintainer" || $role_name =="SuperMaintainer"){
			$fieldset->addField('deployment', 'select', array(
				'label'        => Mage::helper('maintain')->__('Triển khai'),
				'name'        => 'deployment',
				'values'    => array(
				  array(
					  'value'     => 'Yes',
					  'label'     => Mage::helper('maintain')->__('Yes'),
					),
			
				  array(
					  'value'     => 'No',
					  'label'     => Mage::helper('maintain')->__('No'),
					),
				),
			));
		}
		if($role_name =="Administrators" || $role_name =="SuperMaintainer"){
			$fieldset->addField('created_at', 'date', array(
				'name'      => 'created_at',
				'label'     => Mage::helper('maintain')->__('Created Date'),
				'image'     => $this->getSkinUrl('images/grid-cal.gif'),
				'format'    => 'yyyy-M-d',
				'class'     => 'required-entry',
				'required'  => true,
			));
		}
        $form->setValues($data);
        return parent::_prepareForm();
    }
}