<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Maintain Edit Block
 * 
 * @category     Magestore
 * @package     Magestore_Maintain
 * @author      Magestore Developer
 */
class Hchomecenter_Maintain_Block_Adminhtml_Maintain_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'maintain';
        $this->_controller = 'adminhtml_maintain';
        
        $this->_updateButton('save', 'label', Mage::helper('maintain')->__('Save Maintain'));
        $this->_updateButton('delete', 'label', Mage::helper('maintain')->__('Delete Maintain'));
        
        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('maintain_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, 'maintain_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, 'maintain_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('maintain_data')
            && Mage::registry('maintain_data')->getId()
        ) {
            return Mage::helper('maintain')->__("Edit Maintain",
                                                $this->htmlEscape(Mage::registry('maintain_data')->getProductName())
            );
        }
        return Mage::helper('maintain')->__('Add Maintain');
    }
}