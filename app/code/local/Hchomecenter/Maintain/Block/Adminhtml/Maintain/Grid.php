<?php
	/**
		* Magestore
		* 
		* NOTICE OF LICENSE
		* 
		* This source file is subject to the Magestore.com license that is
		* available through the world-wide-web at this URL:
		* http://www.magestore.com/license-agreement.html
		* 
		* DISCLAIMER
		* 
		* Do not edit or add to this file if you wish to upgrade this extension to newer
		* version in the future.
		* 
		* @category    Magestore
		* @package     Magestore_Maintain
		* @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
		* @license     http://www.magestore.com/license-agreement.html
	*/
	
	/**
		* Maintain Grid Block
		* 
		* @category    Magestore
		* @package     Magestore_Maintain
		* @author      Magestore Developer
	*/
	class Hchomecenter_Maintain_Block_Adminhtml_Maintain_Grid extends Mage_Adminhtml_Block_Widget_Grid
	{
		public function __construct()
		{
			parent::__construct();
			$this->setId('maintainGrid');
			$this->setDefaultSort('maintain_id');
			$this->setDefaultDir('DESC');
			$this->setSaveParametersInSession(true);
		}
		
		/**
			* prepare collection for block to display
			*
			* @return Hchomecenter_Maintain_Block_Adminhtml_Maintain_Grid
		*/
		protected function _prepareCollection()
		{
			$collection = Mage::getModel('maintain/maintain')->getCollection();
			$this->setCollection($collection);
			return parent::_prepareCollection();
		}
		
		/**
			* prepare columns for this grid
			*
			* @return Hchomecenter_Maintain_Block_Adminhtml_Maintain_Grid
		*/
		protected function _prepareColumns()
		{
			$this->addColumn('maintain_id', array(
            'header'    => Mage::helper('maintain')->__('ID'),
            'align'     =>'right',
            'width'     => '30px',
            'index'     => 'maintain_id',
			));
			
			$this->addColumn('manufacture', array(
            'header'    => Mage::helper('maintain')->__('Hãng'),
            'align'     =>'left',
            'index'     => 'manufacture',
			));
			
			$this->addColumn('maintain_info', array(
            'header'    => Mage::helper('maintain')->__('Thông tin'),
            'align'     =>'left',
            'index'     => 'maintain_info',
			));	
			
			$this->addColumn('customer_name', array(
            'header'    => Mage::helper('maintain')->__('Họ tên'),
            'align'     =>'left',
            'index'     => 'customer_name',
			));
			
			$this->addColumn('mobile', array(
            'header'    => Mage::helper('maintain')->__('Mobile'),
            'align'     =>'left',
            'index'     => 'mobile',
			)); 
			
			$this->addColumn('email', array(
            'header'    => Mage::helper('maintain')->__('Email'),
			'align'     =>'left',
            'index'     => 'email',
			));
			
			$this->addColumn('address', array(
            'header'    => Mage::helper('maintain')->__('Địa chỉ'),
			'align'     =>'left',
            'index'     => 'address',
			));
			
			$this->addColumn('supporter_call', array(
			'header'    => Mage::helper('maintain')->__('Tình trạng CSKH gọi'),
			'align'     =>'left',
			'index'     => 'supporter_call',
			));
			
			$this->addColumn('appointment_time', array(
			'header'    => Mage::helper('maintain')->__('Ngày đã hẹn'),
			'align'     =>'left',
			'index'     => 'appointment_time',
			));
			
			$this->addColumn('deployment', array(
			'header'    => Mage::helper('maintain')->__('Triển khai'),
			'align'     =>'left',
			'index'     => 'deployment',
			));
			
			$this->addColumn('source_register', array(
			'header'    => Mage::helper('maintain')->__('Nguồn đăng ký'),
			'align'     =>'left',
			'index'     => 'source_register',
			));
			
			$this->addColumn('created_at', array(
            'header'    => Mage::helper('maintain')->__('Created Date'),
            'align'     =>'left',
            'index'     => 'created_at',
			'type'      => 'date',
			'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
			)); 
			
			$this->addColumn('action',
            array(
			'header'    =>    Mage::helper('maintain')->__('Action'),
			'width'        => '100',
			'type'        => 'action',
			'getter'    => 'getId',
			'actions'    => array(
			array(
			'caption'    => Mage::helper('maintain')->__('Edit'),
			'url'        => array('base'=> '*/*/edit'),
			'field'        => 'id'
			)),
			'filter'    => false,
			'sortable'    => false,
			'index'        => 'stores',
			'is_system'    => true,
			));
			return parent::_prepareColumns();
		}
		
		/**
			* prepare mass action for this grid
			*
			* @return Hchomecenter_Maintain_Block_Adminhtml_Maintain_Grid
		*/
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('maintain_id');
			$this->getMassactionBlock()->setFormFieldName('maintain');
			
			$this->getMassactionBlock()->addItem('delete', array(
            'label'        => Mage::helper('maintain')->__('Delete'),
            'url'        => $this->getUrl('*/*/massDelete'),
            'confirm'    => Mage::helper('maintain')->__('Are you sure?')
			));
			return $this;
		}
		
		/**
			* get url for each row in grid
			*
			* @return string
		*/
		public function getRowUrl($row)
		{
			return $this->getUrl('*/*/edit', array('id' => $row->getId()));
		}
	}	