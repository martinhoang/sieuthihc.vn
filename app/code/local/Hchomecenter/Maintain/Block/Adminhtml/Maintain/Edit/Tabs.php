<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Maintain Edit Tabs Block
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @author      Magestore Developer
 */
class Hchomecenter_Maintain_Block_Adminhtml_Maintain_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('maintain_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('maintain')->__('Maintain Information'));
    }
    
    /**
     * prepare before render block to html
     *
     * @return Hchomecenter_Maintain_Block_Adminhtml_Maintain_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('maintain')->__('Maintain Information'),
            'title'     => Mage::helper('maintain')->__('Maintain Information'),
            'content'   => $this->getLayout()
                                ->createBlock('maintain/adminhtml_maintain_edit_tab_form')
                                ->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}