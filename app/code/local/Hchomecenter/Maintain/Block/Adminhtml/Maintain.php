<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Maintain Adminhtml Block
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @author      Magestore Developer
 */
class Hchomecenter_Maintain_Block_Adminhtml_Maintain extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_maintain';
        $this->_blockGroup = 'maintain';
        $this->_headerText = Mage::helper('maintain')->__('Maintain Manager');
        $this->_addButtonLabel = Mage::helper('maintain')->__('Add Maintain');
        parent::__construct();
    }
}