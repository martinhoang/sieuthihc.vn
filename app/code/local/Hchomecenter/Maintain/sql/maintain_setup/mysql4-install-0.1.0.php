<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Maintain
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create maintain table
 */
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('maintain')};

CREATE TABLE {$this->getTable('maintain')} (
  `maintain_id` int(11) unsigned NOT NULL auto_increment,
  `manufacture` varchar(255) NOT NULL default '',
  `maintain_info` varchar(255) NOT NULL default '',
  `customer_name` varchar(255) NOT NULL default '',
  `mobile` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `address` varchar(255) NOT NULL default '',
  `created_at` datetime NULL,
  PRIMARY KEY (`maintain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

