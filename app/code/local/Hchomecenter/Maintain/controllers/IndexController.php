<?php
	/**
		* Magestore
		* 
		* NOTICE OF LICENSE
		* 
		* This source file is subject to the Magestore.com license that is
		* available through the world-wide-web at this URL:
		* http://www.magestore.com/license-agreement.html
		* 
		* DISCLAIMER
		* 
		* Do not edit or add to this file if you wish to upgrade this extension to newer
		* version in the future.
		* 
		* @category    Magestore
		* @package     Magestore_Maintain
		* @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
		* @license     http://www.magestore.com/license-agreement.html
	*/
	
	/**
		* Maintain Index Controller
		* 
		* @category    Magestore
		* @package     Magestore_Maintain
		* @author      Magestore Developer
	*/
	class Hchomecenter_Maintain_IndexController extends Mage_Core_Controller_Front_Action
	{
		/**
			* index action
		*/
		public function indexAction()
		{
			$this->loadLayout();
			$this->renderLayout();
		}
		public function saveAction(){
			$session = Mage::getSingleton('core/session');
			$data = $this->getRequest()->getPost();
			if($data['serial_no']==null){
				$maintain_info = " Thông tin gồm có: Số phiếu biên nhận: ".$data['num_invoice']." + "
				." Model: ".$data['model']." + "
				." Số điện thoại khi đăng ký mua hàng: ".$data['buymobile']." + "
				." Ngày hẹn của khách hàng: ".$data['cal_date'];
			}
			else{
				$maintain_info = " Thông tin gồm có: Số phiếu biên nhận: ".$data['num_invoice']." + "
				." Model: ".$data['model']." + "
				." Số máy/ Serial No ( nếu có): ".$data['serial_no']." + "
				." Số điện thoại khi đăng ký mua hàng: ".$data['buymobile'];
			}
			if ($data) {
				$model = Mage::getModel('maintain/maintain');
				$model->setManufacture($data['paper_type'])
				->setSex($data['sex'])
				->setCustomerName($data['customer_name'])
				->setMobile($data['mobile'])
				->setEmail($data['email'])
				->setAddress($data['address'])
				->setMaintainInfo($maintain_info)
				->setSourceRegister('Đăng ký Online qua Website')
				->setDeployment('No')
				->setSupporterCall('Chưa gọi')
				->setCreatedAt(now());
				$model->save();
				$session->addSuccess($this->__('Bạn đã gửi đăng ký thành công'));
				if(isset($model)){
					$recipientName = Mage::getStoreConfig('maintain/general/recipient_name', $store);
					$recipientEmail = Mage::getStoreConfig('maintain/general/recipient_email', $store);
					$store = Mage::app()->getStore();
					$translate = Mage::getSingleton('core/translate');
					$translate->setTranslateInline(false);
					
					Mage::getModel('core/email_template')
					->setDesignConfig(array(
					'area' => 'frontend',
					'store' => $store->getId()
					))->sendTransactional(
					Mage::getStoreConfig('maintain/general/email', $store),
					Mage::getStoreConfig('trans_email/ident_general', $store),
					$recipientEmail,
					$recipientName,
					array(
					'store' => $store,
					'recipient_name' => $recipientName,
					'recipient_email' => $recipientEmail,
					'customer_name' => $data['customer_name'],
					'email' => $data['email'],
					'address' => $data['address'],
					'sex' => $data['sex'],
					'mobile' => $data['mobile'],
					'maintain_info' => $maintain_info
					)
					);
					$translate->setTranslateInline(true);
					$this->loadLayout();
					$this->renderLayout();
				}
			}	
		}
	}																	