<?php

class Vintex_Export_Model_Observer {
    
    public function includeOption($observer)
    {
        // Get code of grid
        $idBlockObserver = $observer->getEvent()->getBlock()->getId();

        if ($idBlockObserver=="sales_order_grid" ) {
            
            // copy+paste by "delete orders" module, thanks TienHoang
            $block = $observer->getEvent()
                ->getBlock()
                ->getMassactionBlock();
            
            if ($block) {
                $block->addItem('export', array(
                    'label'=> Mage::helper('export')->__('Export Comment History'),
                    'url'  => Mage::getUrl('comment_export', array('_secure'=>true)),
                ));
            }
        }
    }

}