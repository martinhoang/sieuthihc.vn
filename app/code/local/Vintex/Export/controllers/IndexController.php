<?php


require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';

class Vintex_Export_IndexController 
    extends Mage_Adminhtml_Sales_OrderController {

        const FILENAME = 'comment_history.csv';

        public function indexAction()
        {
            
            $post = $this->getRequest()->getPost();

            $orderIdsList = $post['order_ids'];

             $serviceGenerateCSV = new Vintex_Export_Service_GenerateCSV($orderIdsList);
             $contentCSV = $serviceGenerateCSV->call();

             $this->_prepareDownloadResponse(self::FILENAME, $contentCSV);               
        }
    }