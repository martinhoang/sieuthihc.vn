<?php
	
	class Vintex_Export_Service_GenerateCSV {
		
		private $_orderIds;
		private $_collectionOrders;
		private $_contentCSV;
		
		public function __construct($ordersId) {
			$this->_orderIds = $ordersId;
		}		
		private function _loadOrderObjects()
		{
			$this->_collectionOrders = array();
			
			foreach($this->_orderIds as $id) {
				$instance = Mage::getModel("sales/order")->load($id);
				array_push($this->_collectionOrders, $instance);
			}
		}	
		private function _prepareData($templateLine)
		{
			$this->_contentCSV = "";
			//iterate on the orders selected
			foreach($this->_collectionOrders as $order) {
				$create_time =Mage::helper('core')->formatDate($order->getCreatedAtDate(), 'medium', true);
				$commentsObject = $order->getStatusHistoryCollection(true);
				foreach ($commentsObject as $commentObj) {
					$date = Mage::helper('core')->formatDate($commentObj->getCreatedAtDate(), 'medium');
					$time = Mage::helper('core')->formatTime($commentObj->getCreatedAtDate(), 'medium');
					$lineItem = "";
					
					// iterate on the itens in template
					foreach($templateLine as $t) {
						$item = "";
						list($object, $attribute) = explode(".", $t);
						
						switch($object) {
							
							case "order":
							if ($attribute=="created_at"){
								$item = $create_time;
							}
							if($attribute=="increment_id"){
								$item = $order->getIncrementId();
							}
							break;
							
							case "comment":
							if ($attribute=="status"){
								$item = $commentObj->getStatusLabel();
							}
							if ($attribute=="content"){
								$item = $commentObj->getComment();
							}
							if ($attribute=="created_at"){
								$item = $date." ".$time;
							}
							break;
						}
						$lineItem.="{$item},";
					}
					// endline
					$this->_contentCSV .=$lineItem ."\n";
				}
				$memo = $order->getCreditmemosCollection();
				if(count($memo)>0){
					foreach ($memo as $_memo){
						$date = Mage::helper('core')->formatDate($_memo->getCreatedAtDate(), 'medium');
						$time = Mage::helper('core')->formatTime($_memo->getCreatedAtDate(), 'medium');
						$lineItem = "";
						// iterate on the itens in template
						foreach($templateLine as $t) {
							$item = "";
							list($object, $attribute) = explode(".", $t);
							switch($object) {
								case "order":
								if ($attribute=="created_at"){
									$item = $create_time;
								}
								if($attribute=="increment_id"){
									$item = $order->getIncrementId();
								}
								break;
								
								case "comment":
								if ($attribute=="status"){
									$item = 'Credit memo '.$_memo->getIncrementId().' created';
								}
								if ($attribute=="content"){
									foreach ($_memo->getCommentsCollection() as $_comment){
										$item = $_comment->getComment();
									}
								}
								if ($attribute=="created_at"){
									$item = $date." ".$time;
								}
								break;
							}
							$lineItem.="{$item},";
						}
						// endline
						$this->_contentCSV .=$lineItem ."\n";
					}
				}
				$shipment = $order->getShipmentsCollection();
				if(count($shipment)>0){
					foreach ($shipment as $_shipment){
						$date = Mage::helper('core')->formatDate($_shipment->getCreatedAtDate(), 'medium');
						$time = Mage::helper('core')->formatTime($_shipment->getCreatedAtDate(), 'medium');
						$lineItem = "";
						// iterate on the itens in template
						foreach($templateLine as $t) {
							$item = "";
							list($object, $attribute) = explode(".", $t);
							switch($object) {
								case "order":
								if ($attribute=="created_at"){
									$item = $create_time;
								}
								if($attribute=="increment_id"){
									$item = $order->getIncrementId();
								}
								break;
								
								case "comment":
								if ($attribute=="status"){
									$item = 'Shipment memo '.$_shipment->getIncrementId().' created';
								}
								if ($attribute=="content"){
									foreach ($_shipment->getCommentsCollection() as $_comment){
										$item = $_comment->getComment();
									}
								}
								if ($attribute=="created_at"){
									$item = $date." ".$time;
								}
								break;
							}
							$lineItem.="{$item},";
						}
						// endline
						$this->_contentCSV .=$lineItem ."\n";
					}
				}
				$invoice = $order->getInvoiceCollection();
				if(count($invoice)>0){
					foreach ($invoice as $_invoice){
						$date = Mage::helper('core')->formatDate($_invoice->getCreatedAtDate(), 'medium');
						$time = Mage::helper('core')->formatTime($_invoice->getCreatedAtDate(), 'medium');
						$lineItem = "";
						// iterate on the itens in template
						foreach($templateLine as $t) {
							$item = "";
							list($object, $attribute) = explode(".", $t);
							switch($object) {
								case "order":
								if ($attribute=="created_at"){
									$item = $create_time;
								}
								if($attribute=="increment_id"){
									$item = $order->getIncrementId();
								}
								break;
								
								case "comment":
								if ($attribute=="status"){
									$item = 'Shipment memo '.$_invoice->getIncrementId().' created';
								}
								if ($attribute=="content"){
									foreach ($_invoice->getCommentsCollection() as $_comment){
										$item = $_comment->getComment();
									}
								}
								if ($attribute=="created_at"){
									$item = $date." ".$time;
								}
								break;
							}
							$lineItem.="{$item},";
						}
						// endline
						$this->_contentCSV .=$lineItem ."\n";
					}
				}
			}
		}
		
		public function call()
		{
			$this->_loadOrderObjects();
			
			$templateLine = Mage::helper("export")->loadTemplate();
			
			$this->_prepareData($templateLine);
			
			return $this->_contentCSV;
		}
		
	}																