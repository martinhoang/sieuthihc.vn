<?php
class VTC_Pay_Block_Info_Pay extends Mage_Payment_Block_Info
{
    protected $_receiverAcc;
    protected $_websiteId;
    protected $_secretKey;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payment/info/pay.phtml');
	
    }
    public function getReceverAcc()
    {
        if (is_null($this->_receiverAcc)) {
            $this->_convertAdditionalData();
        }
        return $this->_receiverAcc;
    }
    public function getWebsiteId()
    {
    	if (is_null($this->_websiteId)) {
            $this->_convertAdditionalData();
        }
        return $this->_websiteId;
    }	
    public function getSecretKey()
    {
    	if (is_null($this->_secretKey)) {
            $this->_convertAdditionalData();
        }
        return $this->_secretKey;
    }
    /**
     * Enter description here...
     *
     * @return Mage_Payment_Block_Info_Checkmo
     */
    protected function _convertAdditionalData()
    {
		$info = $this->getInfo();
		var_dump($info);
        $details = @unserialize($this->getInfo()->getAdditionalData());
        if (is_array($details)) {
            $this->_receiverAcc = isset($details['receiver_acc']) ? (string) $details['receiver_acc'] : '';
            $this->_websiteId = isset($details['website_ID']) ? (string) $details['website_ID'] : '';			
            $this->_secretKey = isset($details['secret_key']) ? (string) $details['secret_key'] : '';
        } 
		else {
            $this->_receiverAcc = '';
            $this->_websiteId='';
            $this->_secretKey='';
        }
        return $this;
    }
    
    public function toPdf()
    {
        $this->setTemplate('payment/info/pdf/pay.phtml');
        return $this->toHtml();
    }

}
?>