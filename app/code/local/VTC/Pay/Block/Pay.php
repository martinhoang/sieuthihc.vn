<?php
class VTC_Pay_Block_Pay extends Mage_Checkout_Block_Onepage_Success
{	
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getVTCPay()     
     { 
        if (!$this->hasData('pay')) {
            $this->setData('pay', Mage::registry('pay'));
        }
        return $this->getData('pay');      
   	 } 
	 public function vtc_price($price){
		$price_vtc	= strip_tags($price);
		$price_vtc 	= str_replace(',','',$price_vtc);
		$price_vtc 	= str_replace('.','',$price_vtc);
		$price_vtc 	= str_replace('VN�','',$price_vtc);
		$price_vtc 	= trim($price_vtc);
		return $price_vtc;
	}
	public function requestCheckoutUrl($website_ID,$order_id,$total_amount, $receiver_acc,$secret_key)
	{
		//$destinationUrl="http://sandbox1.vtcebank.vn/pay.vtc.vn/gate/checkout.html" ; //link test
		$destinationUrl="https://pay.vtc.vn/cong-thanh-toan/checkout.html"; //link th?t
		$payment_method =1;
		if ($order_id != '')
		{
			$order_code = $order_id;
		
		} else $order_code = strval(time() . "-" . $order_id);

		$vtcpay_total =$total_amount;				
		$plaintext = $website_ID . "-" . $payment_method . "-" . $order_code . "-" . $vtcpay_total . "-" . $receiver_acc . "-" . "" . "-". $secret_key;
		
		$sign = strtoupper(hash('sha256', $plaintext));
		
		$data = "?website_ID=" . $website_ID . "&payment_method=" . $payment_method . "&order_code=" . $order_code . "&amount=" .$vtcpay_total . "&receiver_acc=" .  $receiver_acc;
		
		$data = $data . "&customer_name=" . "". "&customer_mobile=" . "" . "&order_des=" . "" . "&param_extend=" . "" . "&sign=" . $sign;		

		$destinationUrl = $destinationUrl . $data;
		return $destinationUrl;
	}	
	public function thanhtoanvtcpay(){

		$receiver_acc= strval($_SESSION['receiver_acc']);
		$website_ID= $_SESSION['website_ID'];
		$secret_key= strval($_SESSION['secret_key']);
		
		$order_id =strval(Mage::getSingleton('checkout/session')->getLastRealOrderId());
		$order = Mage::getModel('sales/order')->loadByIncrementId($this->getOrderId());
		$total_amount= $order->getTotalDue();
		//$total_amount=$this->vtc_price($_SESSION['total_amount']); 
		$total_amount = intval($total_amount);
		unset($_SESSION['receiver_acc']);
		unset($_SESSION['website_ID']);
		unset($_SESSION['secret_key']);
		
		return $this->requestCheckoutUrl($website_ID, $order_id, $total_amount, $receiver_acc, $secret_key);
	}	
}
?>