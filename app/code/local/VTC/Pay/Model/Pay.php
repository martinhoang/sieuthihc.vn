<?php
class VTC_Pay_Model_Pay extends Mage_Payment_Model_Method_Abstract
{
	protected $_code ='pay';
	protected $_formBlockType = 'pay/form_pay';
	protected $_infoBlockType = 'pay/info_pay';
	public function assignData($data)
    {
        $details = array();
		
        if ($this->getReceverAcc()) {
            $details['receiver_acc'] = $this->getReceverAcc();
        }
        if($this->getWebsiteId()){
        	$details['website_ID']=$this->getWebsiteId();
        }
        if($this->getSecretKey()){
        	$details['secret_key']=$this->getSecretKey();
        }
        if (!empty($details)) {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }
        return $this;
    }	
    public function getWebsiteId()
    {
    	return $this->getConfigData('website_ID');
    }
    public function getSecretKey()
    {
    	return $this->getConfigData('secret_key');
    }
	    public function getReceverAcc()
    {
        return $this->getConfigData('receiver_acc');
    }
}
?>
