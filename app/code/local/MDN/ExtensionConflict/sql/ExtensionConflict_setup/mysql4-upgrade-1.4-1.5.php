<?php
$installer = $this;

$installer->startSetup();
$installer->addAttribute(
    'catalog_product',
    'show_from_date',
    array(
		'group' => 'Prices',
		'label' => 'Price Show From Date',
		'type'  => 'datetime',
		'input' => 'date',
		'backend' => '',
		'frontend' => '',
		'visible' => 1,
		'user_defined' => 1,
		'used_for_price_rules' => 1,
		'searchable' => 1,
		'position' => 3,
		'unique' => 0,
		'default' => '',
		'global'  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'sort_order' => 86
    )
);
$installer->addAttribute(
    'catalog_product',
    'show_to_date',
    array(
		'group' => 'Prices',
		'label' => 'Price Show To Date',
		'type'  => 'datetime',
		'input' => 'date',
		'backend' => '',
		'frontend' => '',
		'visible' => 1,
		'user_defined' => 1,
		'used_for_price_rules' => 1,
		'searchable' => 1,
		'position' => 4,
		'unique' => 0,
		'default' => '',
		'global'  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'sort_order' => 87
    )
);
$installer->endSetup();